const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const PORT = process.env.PORT || 4200

const Servicios = require('./Servicios');


app.use(cors())
app.use(express.json());

var requestTime = function(req,res,next){   
    let servicio = req.body.servicio
    let accion = req.body.accion
    let campos = req.body.campos  
    if(servicio == 's3')
    {       
        if(accion == 'upload')
        {
           Servicios.S3_Archivos(campos,'/s3/upload','POST',res)
        }else if(accion == 'subirarchivo')
        {
            Servicios.S3_Archivos(campos,'/s3/subirarchivo','POST',res)
        }else if(accion == 'verarchivos')
        {
            Servicios.S3_Archivos(campos,'/s3/verarchivos','POST',res)
        }else if(accion == 'obtenerURL')
        {
            Servicios.S3_Archivos(campos,'/s3/obtenerURL','POST',res)
        }else if(accion == 'crearnuevoarchivosinrepetir')
        {
            Servicios.S3_Archivos(campos,'/s3/crearnuevoarchivosinrepetir','POST',res)
        }else if(accion == 'eliminararchivo')
        {
            Servicios.S3_Archivos(campos,'/s3/eliminararchivo','POST',res)
        }else if(accion == 'moverlosarchivos')
        {
            Servicios.S3_Archivos(campos,'/s3/moverlosarchivos','POST',res)
        }else if(accion == 'moverpapelera')
        {
            Servicios.S3_Archivos(campos,'/s3/moverpapelera','POST',res)
        }else if(accion == 'eliminararchivopapelera')
        {
            Servicios.S3_Archivos(campos,'/s3/eliminararchivopapelera','POST',res)
        }else if(accion == 'crearpapelera')
        {
            Servicios.S3_Carpetas(campos,'/s3/crearpapelera','POST',res)
        }else if(accion == 'carpetaroot')
        {
            Servicios.S3_Carpetas(campos,'/s3/carpetaroot','POST',res)
        }else if(accion == 'crearcarpetasenlaroot')
        {
            Servicios.S3_Carpetas(campos,'/s3/crearcarpetasenlaroot','POST',res)
        }else if(accion == 'crearcarpetanueva')
        {
            Servicios.S3_Carpetas(campos,'/s3/crearcarpetanueva','POST',res)
        }else if(accion == 'renamefileTODO')
        {
            Servicios.S3_Carpetas(campos,'/s3/renamefileTODO','POST',res)
        }else 
        {
            res.status(409).send({ message: "Accion no encontrada" })
        }
    }else if(servicio == 'archivo')
    {
        if(accion == 'save')
        {
            Servicios.Carpetas_Archivos(campos,'/ddb/archivo/save','POST',res)
        }else if(accion == 'update')
        {
            Servicios.Carpetas_Archivos(campos,'/ddb/archivo/update','PUT',res)
        }else if(accion == 'delete')
        {
            Servicios.Carpetas_Archivos(campos,'/ddb/archivo/delete','DELETE',res)
        }else if(accion == 'ver')
        {
            Servicios.Carpetas_Archivos(campos,'/ddb/archivo/ver','POST',res)
        }else if(accion == 'all')
        {
            Servicios.Carpetas_Archivos(campos,'/ddb/archivo/all','POST',res)
        }else
        {
            res.status(409).send({ message: "Accion no encontrada" })
        }
    }else if(servicio == 'carpeta')
    {
        if(accion == 'save')
        {
            Servicios.Carpetas_Archivos(campos,'/ddb/carpeta/save','POST',res)
        }else if(accion == 'update')
        {
            Servicios.Carpetas_Archivos(campos,'/ddb/carpeta/update','PUT',res)
        }else if(accion == 'delete')
        {
            Servicios.Carpetas_Archivos(campos,'/ddb/carpeta/delete','DELETE',res)
        }else if(accion == 'ver')
        {
            Servicios.Carpetas_Archivos(campos,'/ddb/carpeta/ver','POST',res)
        }else if(accion == 'all')
        {
            Servicios.Carpetas_Archivos(campos,'/ddb/carpeta/all','POST',res)
        }else
        {
            res.status(409).send({ message: "Accion no encontrada" })
        }
    }else if(servicio == 'usuario')
    {
        if(accion == 'save')
        {
            Servicios.Usuario(campos,'/ddb/person/save','POST',res)
        }else if(accion == 'update')
        {
            Servicios.Usuario(campos,'/ddb/usuario/update','PUT',res)
        }else if(accion == 'delete')
        {
            Servicios.Usuario(campos,'/ddb/usuario/delete','DELETE',res)
        }else if(accion == 'login')
        {
            Servicios.Usuario(campos,'/ddb/person/login','POST',res)
        }else if(accion == 'perfil')
        {
            Servicios.Usuario(campos,'/ddb/person/perfil','POST',res)
        }else if(accion == 'updateAuth')
        {
            Servicios.Usuario(campos,'/ddb/usuario/updateAuth','PUT',res)
        }else if(accion == 'Autenticacion')
        {
            Servicios.Usuario(campos,'/ddb/usuario/Autenticacion','POST',res)
        }else
        {
            res.status(409).send({ message: "Accion no encontrada" })
        }
    }else {
        res.status(409).send({ message: "Servicio no encontrado" })
    }
    next();
};

app.use(requestTime);

app.post('/',(req,res) => {
    console.log("Middleware Activado")   
})



app.listen(PORT,() => console.log("Servidor funcionando en puerto",PORT))