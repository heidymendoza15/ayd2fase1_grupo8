const fetch = require("node-fetch");

function Usuario(data,endpoint,metodo,res)
{
    let url = 'http://3.137.212.9:3000'+ endpoint;
    fetch(url, {
        method: metodo, // or 'PUT'
        body: JSON.stringify(data), // data can be `string` or {object}!
        headers:{
          'Content-Type': 'application/json'
        }
      }).then(function(serverPromise){ 
        serverPromise.json()
          .then(function(j) {             
            res.send(j)
          })
      })
      .catch(function(e){
          console.log(e);
        });
}

function S3_Archivos(data,endpoint,metodo,res)
{
    let url = 'http://3.137.212.9:5000'+ endpoint;
    fetch(url, {
        method: metodo, // or 'PUT'
        body: JSON.stringify(data), // data can be `string` or {object}!
        headers:{
          'Content-Type': 'application/json'
        }
      }).then(function(serverPromise){ 
        serverPromise.json()
          .then(function(j) {             
            res.send(j)
          })
      })
      .catch(function(e){
          console.log(e);
        });
}

function S3_Carpetas(data,endpoint,metodo,res)
{
    let url = 'http://3.137.212.9:2000'+ endpoint;
    fetch(url, {
        method: metodo, // or 'PUT'
        body: JSON.stringify(data), // data can be `string` or {object}!
        headers:{
          'Content-Type': 'application/json'
        }
      }).then(function(serverPromise){ 
        serverPromise.json()
          .then(function(j) {             
            res.send(j)
          })
      })
      .catch(function(e){
          console.log(e);
        });
}

function Carpetas_Archivos(data,endpoint,metodo,res)
{
    let url = 'http://3.137.212.9:8080'+ endpoint;
    fetch(url, {
        method: metodo, // or 'PUT'
        body: JSON.stringify(data), // data can be `string` or {object}!
        headers:{
          'Content-Type': 'application/json'
        }
      }).then(function(serverPromise){ 
        serverPromise.json()
          .then(function(j) {             
            res.send(j)
          })
      })
      .catch(function(e){
          console.log(e);
        });
}


module.exports = {
  Usuario,
  S3_Archivos,
  S3_Carpetas,
  Carpetas_Archivos,

}