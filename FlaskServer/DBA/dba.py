import re
import boto3
import logging
from botocore.exceptions import ClientError
from boto3.dynamodb.conditions import Key
from flask import Flask, request
from waitress import serve
import json
import logging
import creds
import random

app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'dba'


@app.route('/ddb/carpeta/save', methods = ['POST'])
def ddb_saveCarpeta():
    if request.method == 'POST':
        content = request.get_json()
        nombre = content['nombre']
        fecha = content['fecha']
        cantidad= content['cantidad']
        propietario= content['propietario']
        numeror=str(random.randint(0,500))
        dynamodb_client = boto3.client(
            'dynamodb',
            aws_access_key_id=creds.dynamodb['access_key_id'],
            aws_secret_access_key=creds.dynamodb['secret_access_key'],
            region_name=creds.dynamodb['region'],
        )

        try:
            response = dynamodb_client.put_item(
                TableName='carpetas',
                Item={
                    'codigo':{'S':numeror},
                    'nombre': {'S': nombre},
                    'fecha': {'S': fecha},
                    'cantidad': {'S': cantidad},
                    'propietario': {'S': propietario}
                },
            )
            logging.info(response)
            x = "{ \"RESPUESTA\": \"EXITOSO/\"}"
            return json.loads(x)
        except ClientError as e:
            logging.error(e)
            x = "{ \"RESPUESTA\": \"FALLO/\"}"
            return json.loads(x) 

@app.route('/ddb/archivo/save', methods = ['POST'])
def ddb_saveArchivo():
    if request.method == 'POST':
        content = request.get_json()
        nombre = content['nombre']
        fecha = content['fecha']
        carpeta= content['carpeta']
        propietario= content['propietario']
        link = content['link']
        extension=content['extension']
        numeror=str(random.randint(0,500))
        dynamodb_client = boto3.client(
            'dynamodb',
            aws_access_key_id=creds.dynamodb['access_key_id'],
            aws_secret_access_key=creds.dynamodb['secret_access_key'],
            region_name=creds.dynamodb['region'],
        )

        try:
            response = dynamodb_client.put_item(
                TableName='archivos',
                Item={
                    'codigo':{'S':numeror},
                    'nombre': {'S': nombre},
                    'fecha': {'S': fecha},
                    'carpeta': {'S': carpeta},
                    'propietario': {'S': propietario},
                    'link':{'S': link},
                    'extension':{'S': extension}
                },
            )
            logging.info(response)
            x = "{ \"RESPUESTA\": \"EXITOSO/\"}"
            return json.loads(x) 
        except ClientError as e:
            logging.error(e)
            x = "{ \"RESPUESTA\": \"FALLO/\"}"
            return json.loads(x) 

@app.route('/ddb/carpeta/update', methods = ['PUT'])
def ddb_updateCarpeta():
    if request.method == 'PUT':
        content = request.get_json()
        nombre = content['nombre']
        cantidad = content['cantidad']
        codigo = str(content['codigo'])
        username = str(content['propietario'])


        dynamodb_client = boto3.client(
            'dynamodb',
            aws_access_key_id=creds.dynamodb['access_key_id'],
            aws_secret_access_key=creds.dynamodb['secret_access_key'],
            region_name=creds.dynamodb['region']
        )

        try:
            response = dynamodb_client.update_item(
                TableName='carpetas',
                Key={'codigo': {'S': codigo},
                    'propietario':{'S':username}
                    },
                UpdateExpression='SET nombre= :val1, cantidad= :val2',
                ConditionExpression='codigo = :val3',
                ExpressionAttributeValues={
                    ':val1': {'S': nombre},
                    ':val2': {'S': cantidad},
                    ':val3': {'S':codigo}
                },
                ReturnValues='UPDATED_NEW'
            )
            logging.info(response)
            x = "{ \"RESPUESTA\": \"EXITOSO/\"}"
            return json.loads(x)
        except ClientError as e:
            logging.error(e)
            x = "{ \"RESPUESTA\": \"FALLO/\"}"
            return json.loads(x) 

@app.route('/ddb/archivo/update', methods = ['PUT'])
def ddb_updateArchivo():
    if request.method == 'PUT':
        content = request.get_json()
        nombre = content['nombre']
        link = content['link']
        codigo=str(content['codigo'])
        carpeta=content['carpeta']
        username = str(content['propietario'])


        dynamodb_client = boto3.client(
            'dynamodb',
            aws_access_key_id=creds.dynamodb['access_key_id'],
            aws_secret_access_key=creds.dynamodb['secret_access_key'],
            region_name=creds.dynamodb['region']
        )

        try:
            response = dynamodb_client.update_item(
                TableName='archivos',
                Key={'codigo': {'S': codigo},
                    'propietario':{'S':username}
                    },
                UpdateExpression='SET nombre= :val1, link= :val2, carpeta= :val4',
                ConditionExpression='codigo = :val3',
                ExpressionAttributeValues={
                    ':val1': {'S': nombre},
                    ':val2': {'S': link},
                    ':val3': {'S': codigo},
                    ':val4': {'S': carpeta}
                },
                ReturnValues='UPDATED_NEW'
            )
            logging.info(response)
            x = "{ \"RESPUESTA\": \"EXITOSO/\"}"
            return json.loads(x)
        except ClientError as e:
            logging.error(e)
            x = "{ \"RESPUESTA\": \"FALLO/\"}"
            return json.loads(x) 

@app.route('/ddb/carpeta/delete', methods = ['Delete'])
def ddb_deleteCarpeta():
    if request.method == 'DELETE':
        content = request.get_json()
        codigo = str(content['codigo'])
        username = str(content['propietario'])


        dynamodb_client = boto3.client(
            'dynamodb',
            aws_access_key_id=creds.dynamodb['access_key_id'],
            aws_secret_access_key=creds.dynamodb['secret_access_key'],
            region_name=creds.dynamodb['region']
        )

        try:
            response = dynamodb_client.delete_item(
                TableName='carpetas',
                Key={'codigo': {'S': codigo},
                     'propietario':{'S':username}
                    }
            )
            logging.info(response)
            x = "{ \"RESPUESTA\": \"EXITOSO/\"}"
            return json.loads(x)
        except ClientError as e:
            logging.error(e)
            x = "{ \"RESPUESTA\": \"FALLO/\"}"
            return json.loads(x) 

@app.route('/ddb/archivo/delete', methods = ['Delete'])
def ddb_deleteArchivo():
    if request.method == 'DELETE':
        content = request.get_json()
        codigo = str(content['codigo'])
        username = str(content['propietario'])

        dynamodb_client = boto3.client(
            'dynamodb',
            aws_access_key_id=creds.dynamodb['access_key_id'],
            aws_secret_access_key=creds.dynamodb['secret_access_key'],
            region_name=creds.dynamodb['region']
        )

        try:
            response = dynamodb_client.delete_item(
                TableName='archivos',
                Key={'codigo': {'S': codigo},
                     'propietario':{'S':username}
                },
                ReturnValues='ALL_OLD'
            )
            logging.info(response)
            x = "{ \"RESPUESTA\": \"EXITOSO/\"}"
            return json.loads(x)
        except ClientError as e:
            logging.error(e)
            x = "{ \"RESPUESTA\": \"FALLO/\"}"
            return json.loads(x) 

@app.route('/ddb/archivo/ver', methods = ['POST'])
def ddb_verArchivo():
    if request.method == 'POST':
        content = request.get_json()
        codigo = str(content['codigo'])

        dynamodb = boto3.resource('dynamodb',
        aws_access_key_id=creds.dynamodb['access_key_id'],
        aws_secret_access_key=creds.dynamodb['secret_access_key'],
        region_name=creds.dynamodb['region'],)

        table = dynamodb.Table('archivos')
        response = table.query(
            KeyConditionExpression=Key('codigo').eq(codigo)
        )
        return json.dumps(response['Items'])

@app.route('/ddb/carpeta/ver', methods = ['POST'])
def ddb_verCarpeta():
    if request.method == 'POST':
        content = request.get_json()
        codigo = str(content['codigo'])

        dynamodb = boto3.resource('dynamodb',
        aws_access_key_id=creds.dynamodb['access_key_id'],
        aws_secret_access_key=creds.dynamodb['secret_access_key'],
        region_name=creds.dynamodb['region'],)

        table = dynamodb.Table('carpetas')
        response = table.query(
            KeyConditionExpression=Key('codigo').eq(codigo)
        )
        return json.dumps(response['Items'])


@app.route('/ddb/archivo/all', methods = ['POST'])
def ddb_verArchivos():
    if request.method == 'POST':
        content = request.get_json()
        codigo = str(content['propietario'])

        dynamodb = boto3.resource('dynamodb',
        aws_access_key_id=creds.dynamodb['access_key_id'],
        aws_secret_access_key=creds.dynamodb['secret_access_key'],
        region_name=creds.dynamodb['region'],)

        table = dynamodb.Table('archivos')
        response = table.scan(
            FilterExpression=Key('propietario').eq(codigo)
        )
        print(response['Items'])
        return json.dumps(response['Items'])

@app.route('/ddb/carpeta/all', methods = ['POST'])
def ddb_verCarpetas():
    if request.method == 'POST':
        content = request.get_json()
        codigo = str(content['propietario'])

        dynamodb = boto3.resource('dynamodb',
        aws_access_key_id=creds.dynamodb['access_key_id'],
        aws_secret_access_key=creds.dynamodb['secret_access_key'],
        region_name=creds.dynamodb['region'],)

        table = dynamodb.Table('carpetas')
        response = table.scan(
            FilterExpression=Key('propietario').eq(codigo)
        )
        print(response['Items'])
        return json.dumps(response['Items'])



if __name__ == '__main__':
    serve(app, host="0.0.0.0", port=8080)


