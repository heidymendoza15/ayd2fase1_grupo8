from dba import app
import unittest


class FlaskTestCase(unittest.TestCase):

  
    def testcarpetasave(self):
        tester = app.test_client(self)
        response = tester.post('/ddb/carpeta/save', json={         'nombre': 'carpeta1','fecha': '18/09/2021','cantidad': '12','propietario': 'propietario' })
        print("Procesando prueba unitaria ......")
        print('tester.post(\'/ddb/carpeta/save\', json={         \'nombre\': \'carpeta1\',\'fecha\': \'18/09/2021\',\'cantidad\': \'12\',\'propietario\': \'propietario\' })')   
        print("EVALUANDO........") 
        print("self.assertIn(b'{\"RESPUESTA\":\"EXITOSO\"}\', response.data)")   
        self.assertIn(b'{"RESPUESTA":"EXITOSO/"}\n', response.data)

    def testarchivosave(self):
        tester = app.test_client(self)
        response = tester.post('/ddb/archivo/save', json={         'nombre': 'archovp1','fecha': '18/09/2021','carpeta': 'caperta1','propietario': 'propietario','link': 'enlace','extension': '.PDF' })
        print("Procesando prueba unitaria ......")
        print('tester.post(\'/ddb/archivo/save\', json={ \'nombre\': \'archovp1\',\'fecha\': \'18/09/2021\',\'carpeta\': \'caperta1\',\'propietario\': \'propietario\',\'link\': \'enlace\',\'extension\': \'.PDF\' })')   
        print("EVALUANDO........") 
        print("self.assertIn(b'{\"RESPUESTA\":\"EXITOSO\"}\', response.data)")   
        self.assertIn(b'{"RESPUESTA":"EXITOSO/"}\n', response.data)

    def testverarchivo(self):
        tester = app.test_client(self)
        response = tester.post('/ddb/archivo/ver', json={   'codigo': '1212' })
        print("Procesando prueba unitaria ......")
        print('tester.post(\'/ddb/archivo/ver\', json={   \'codigo\': \'1212\' })')   
        print("EVALUANDO........") 
        print("json_data = response.get_json()")
        print("assert generate_response(codigo, json_data['resp'])")
        json_data = response.get_json()
        if(json_data != ""):
            tempo ="EXITO"
            self.assertIn("EXITO",tempo)

    def testvercarpeta(self):
        tester = app.test_client(self)
        response = tester.post('/ddb/carpeta/ver', json={   'codigo': '1212' })
        print("Procesando prueba unitaria ......")
        print('tester.post(\'/ddb/carpeta/ver\', json={   \'codigo\': \'1212\' })')   
        print("EVALUANDO........") 
        print("json_data = response.get_json()")
        print("assert generate_response(codigo, json_data['Items'])")
        json_data = response.get_json()
        if(json_data != ""):
            tempo ="EXITO"
            self.assertIn("EXITO",tempo)

    def testverarchivosall(self):
        tester = app.test_client(self)
        response = tester.post('/ddb/archivo/all', json={   'propietario': '1212' })
        print("Procesando prueba unitaria ......")
        print('tester.post(\'/ddb/archivo/all\', json={   \'propietario\': \'1212\' })')   
        print("EVALUANDO........") 
        print("json_data = response.get_json()")
        print("assert generate_response(codigo, json_data['Items'])")
        json_data = response.get_json()
        if(json_data != ""):
            tempo ="EXITO"
            self.assertIn("EXITO",tempo)
 

if __name__ == '__main__':
    unittest.main()