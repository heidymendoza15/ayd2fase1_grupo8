from carpeta import app
import unittest


class FlaskTestCase(unittest.TestCase):

  


    def testcarpetaroot(self):
        tester = app.test_client(self)
        response = tester.post('/s3/carpetaroot', json={         'name': 'Temporal' })
        print("Procesando prueba unitaria ......")
        print('tester.post(\'/s3/carpetaroot\', json={         \'name\': \'Temporal\' })')   
        print("EVALUANDO........") 
        print("self.assertIn(b'{\"RESPUESTA\":\"SE CREO CARPETA ROOT/\"}', response.data)")   
        self.assertIn(b'{"RESPUESTA":"SE CREO CARPETA ROOT/"}\n', response.data)

    def testcrearcarpetasenlaroot(self):
        tester = app.test_client(self)
        response = tester.post('/s3/crearcarpetasenlaroot', json={         'name': 'Temporal' ,'carpetas': 'Cesar/temporal' })
        print("Procesando prueba unitaria ......")
        print('tester.post(\'/s3/crearcarpetasenlaroot\', json={         \'name\': \'Temporal\' ,\'carpetas\': \'Cesar/temporal\' })')   
        print("EVALUANDO........") 
        print("self.assertIn(b'{\"RESPUESTA\":\"SE CREO CARPETA EN LA CARPETA ROOT/\"}', response.data)")   
        self.assertIn(b'{"RESPUESTA":"SE CREO CARPETA EN LA CARPETA ROOT/"}\n', response.data)

    def testcrearcarpetanueva(self):
        tester = app.test_client(self)
        response = tester.post('/s3/crearcarpetanueva', json={         'name': 'Temporal','name2':'Cesar/' })
        print("Procesando prueba unitaria ......")
        print('tester.post(\'/s3/carpetaroot\', json={         \'name\': \'Temporal\' })')   
        print("EVALUANDO........") 
        print(response.data)
        print("self.assertIn(b'{\"RESPUESTA\":\"SE CREO /\"}', response.data)")   
        self.assertIn(b'{"RESPUESTA":"ERROR NO  SE CREO/"}\n', response.data)


    def testcrearpapelera(self):
        tester = app.test_client(self)
        response = tester.post('/s3/crearpapelera', json={   'name': 'Cesar'})
        print("Procesando prueba unitaria ......")
        print('tester.post(\'/s3/crearpapelera\', json={   \'name\': \'Cesar\'})')   
        print("EVALUANDO........") 
        print("self.assertIn(b'{\"RESPUESTA\":\"SE CREO CARPETA  DE PAPELERA EN LA CARPETA ROOT /\"}', response.data)")   
        self.assertIn(b'{"RESPUESTA":"SE CREO CARPETA  DE PAPELERA EN LA CARPETA ROOT/"}\n', response.data)
   


if __name__ == '__main__':
    unittest.main()