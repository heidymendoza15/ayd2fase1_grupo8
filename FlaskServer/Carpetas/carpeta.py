import re
import boto3
import logging
import botocore
from botocore.exceptions import ClientError
from boto3.dynamodb.conditions import Key, Attr
from flask import Flask, request
from waitress import serve
import json
import base64
import tempfile
import uuid
import logging
import creds

app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'carpetas'

#CREAR CARPETA DE CADA USUARIO ROOT al momento de login debe crear una carpeta root por usuario YA12
@app.route('/s3/carpetaroot', methods = ['POST'])
def s3_crearcarpeta():
    if request.method == 'POST':
        content = request.get_json()
        name = content['name']
        

        s3_client = boto3.client(
            's3',
            aws_access_key_id=creds.s3['access_key_id'],
            aws_secret_access_key=creds.s3['secret_access_key'],
        )

        BUCKET_NAME = 'bucket-analisis2'
        FOLDER_NAME = 'images'
        directory_name = name #it's name of your folders

        try:
            response = s3_client.put_object(Bucket=BUCKET_NAME, Key=(directory_name+'/'))
            logging.info(response)
            x = "{ \"RESPUESTA\": \"SE CREO CARPETA ROOT/\"}"
            return json.loads(x) 
        except ClientError as e:
            logging.error(e)
            x = "{ \"RESPUESTA\": \"ERROR NO CREO CARPETA ROOT/\"}"
            return json.loads(x) 

#CREAR CARPETAS EN LA CARPETA ROOT podra tener acceso a su carpeta root y crear dentro de ella mas carpetas YA12
@app.route('/s3/crearcarpetasenlaroot', methods = ['POST'])
def s3_crearcarpetasenroot():
    if request.method == 'POST':
        content = request.get_json()
        name = content['name'] #ID = CESAR
        carpetas = content['carpetas'] # DIRECCION carpeta1/carpeta2/  = CESAR/carpeta1/carpeta2/ tiene que llevar slash al final 
        

        s3_client = boto3.client(
            's3',
            aws_access_key_id=creds.s3['access_key_id'],
            aws_secret_access_key=creds.s3['secret_access_key'],
        )

        BUCKET_NAME = 'bucket-analisis2'
        FOLDER_NAME = 'images'
        directory_name = name  #it's name of your folders

        try:
            response = s3_client.put_object(Bucket=BUCKET_NAME, Key=(directory_name+'/'+carpetas))
            x = "{ \"RESPUESTA\": \"SE CREO CARPETA EN LA CARPETA ROOT/\"}"
            return json.loads(x) 
        except ClientError as e:
            logging.error(e)
            x = "{ \"RESPUESTA\": \"ERROR NO SE CREO CARPETA EN LA CARPETA ROOT/\"}"
            return json.loads(x) 


#CREAR CARPETAS EN LA CARPETA ROOT SIN REPETIR YA12
@app.route('/s3/crearcarpetanueva', methods = ['POST'])
def s3_crearcarpetanueva():
   
    if request.method == 'POST':
        bandera = True;
        content = request.get_json()
        name = content['name'] #ID = SIN SLASH  nombre de la carpeta nueva Nombre de la carpeta donde va ir a ver si existe Cesar/ tiene que llevar slash
        name2 = content['name2'] #ID = CESAR/ nombre del root cons slash
       
        

        s3_client = boto3.client(
            's3',
            aws_access_key_id=creds.s3['access_key_id'],
            aws_secret_access_key=creds.s3['secret_access_key'],
        )
        
        s3_client2 = boto3.resource(
            's3',
            aws_access_key_id=creds.s3['access_key_id'],
            aws_secret_access_key=creds.s3['secret_access_key'],
        )

        BUCKET_NAME = 'bucket-analisis2'
        directory_name = name  #it's name of your folders

        #VALIDANDO QUE EXISTA LA CARPETA
        my_bucket = s3_client2.Bucket('bucket-analisis2')
        for object_summary in my_bucket.objects.filter(Prefix=name2):
            #print(object_summary.key)
            temporal = object_summary.key
            separador = "/"
            separado = temporal.split(separador)
            print(separado[1])
            if separado[1]  == name:
                print("YA EXISTE UNA CARPETA CON ESE NOMBRE NO DEBE CREAR")
                bandera=False
                break;
            else:
                print("NO EXISTE CARPETA CON ESE NOMBRE DEBE CREAR")
                bandera=True
        
        BUCKET_NAME = 'bucket-analisis2'
        if bandera:
            try:
                response = s3_client.put_object(Bucket=BUCKET_NAME, Key=(name2+''+name+'/'))
                logging.info(response)
                x = "{ \"RESPUESTA\": \"SI SE CREO/\"}"
                return json.loads(x) 
            except ClientError as e:
                logging.error(e)
                x = "{ \"RESPUESTA\": \"ERROR NO  SE CREO/\"}"
                return json.loads(x) 
                
    x = "{ \"RESPUESTA\": \"ERROR NO  SE CREO/\"}"
    return json.loads(x) 


#RENOMBRAR CARPETAS O ARCHIVOS YA12
@app.route('/s3/renamefileTODO', methods = ['POST'])
def s3_renamefileTODO():
    if request.method == 'POST':
        content = request.get_json()
        direccionviejonombre = content['direccionviejonombre']  #nombre del archivo con extencion ubicacion exacta
        direccionnuevonombre = content['direccionnuevonombre']  #nombre del nuevo archivo con extencion ubicacion exactao
      

        

        s3_client = boto3.resource(
            's3',
            aws_access_key_id=creds.s3['access_key_id'],
            aws_secret_access_key=creds.s3['secret_access_key'],
        )

        my_bucket = s3_client.Bucket('bucket-analisis2')
        # SI SE LOGRO ACCEDER Y COPIAR TODO
        #s3_client.Object('bucket-analisis2', 'Cesar/holamundo.PDF').copy_from(CopySource='Cesar/ejempkkloperrp2.PDF',ACL='public-read')
        copy_source = {
        'Bucket': 'bucket-analisis2',  
        'Key': direccionviejonombre
        }
        s3_client.meta.client.copy(copy_source, 'bucket-analisis2',direccionnuevonombre,ExtraArgs={'ACL':'public-read'})
        # Delete the former object A
        s3_client.Object('bucket-analisis2', direccionviejonombre).delete()
        
        x = "{ \"RESPUESTA\": \"SI SE RENOMBRO/\"}"
        return json.loads(x) 



#==================================================
#==================================================
#================NUEVOS SERCICIOS==================
#==================================================
#==================================================

#CREAR CARPETAS DE RECICLAJE EN LA CARPETA ROOT
@app.route('/s3/crearpapelera', methods = ['POST'])
def s3_crearcarpetapepelerea():
    if request.method == 'POST':
        content = request.get_json()
        name = content['name'] #ID = CESAR
       # carpetas = content['carpetas'] # DIRECCION carpeta1/carpeta2/  = CESAR/carpeta1/carpeta2/ tiene que llevar slash al final 
        

        s3_client = boto3.client(
            's3',
            aws_access_key_id=creds.s3['access_key_id'],
            aws_secret_access_key=creds.s3['secret_access_key'],
        )

        BUCKET_NAME = 'bucket-analisis2'
        directory_name = name  #it's name of your folders

        try:
            response = s3_client.put_object(Bucket=BUCKET_NAME, Key=(directory_name+'/'+'papelera/'))
            x = "{ \"RESPUESTA\": \"SE CREO CARPETA  DE PAPELERA EN LA CARPETA ROOT/\"}"
            return json.loads(x) 
        except ClientError as e:
            logging.error(e)
            x = "{ \"RESPUESTA\": \"ERROR NO SE CREO CARPETA DE PAPELERA EN LA CARPETA ROOT/\"}"
            return json.loads(x) 



if __name__ == '__main__':
    serve(app, host="0.0.0.0", port=2000)
