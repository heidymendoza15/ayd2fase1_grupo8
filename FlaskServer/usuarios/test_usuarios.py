from usuarios import app
import unittest


class FlaskTestCase(unittest.TestCase):

  
    def testcarpetaroot(self):
        tester = app.test_client(self)
        response = tester.post('/ddb/person/save', json={         'id': 'idTemporal','correo': 'coTemporal','password': 'pasTemporal','fecha': 'feTemporal' })
        print("Procesando prueba unitaria ......")
        print('tester.post(\'/ddb/person/save\', json={         \'id\': \'idTemporal\',\'correo\': \'coTemporal\',\'password\': \'pasTemporal\',\'fecha\': \'feTemporal\' })')   
        print("EVALUANDO........") 
        print("self.assertIn(b'{\"RESPUESTA\":\"EXITOSO\"}\', response.data)")   
        self.assertIn(b'{"RESPUESTA":"EXITOSO/"}\n', response.data)

    def testlogin(self):
        tester = app.test_client(self)
        response = tester.post('/ddb/person/login', json={         'id': 'idTemporal','password': 'pasTemporal' })
        print("Procesando prueba unitaria ......")
        print('tester.post(\'/ddb/person/login\', json={  \'id\': \'idTemporal\',\'password\': \'pasTemporal\' })')   
        print("EVALUANDO........") 
        print("self.assertIn(b'{\"RESPUESTA\":\"EXITOSO\"}', response.data)")   
        self.assertIn(b'{"RESPUESTA":"EXITOSO/"}\n', response.data)

    def testperfil(self):
            tester = app.test_client(self)
            response = tester.post('/ddb/person/perfil', json={     'id': 'idTemporal' })
            print("Procesando prueba unitaria ......")
            print('tester.post(\'/ddb/person/perfil\', json={    \'id\': \'idTemporal\' })')   
            print("EVALUANDO222........") 
            print(" self.assertIn(b'[{\"correo\": \"coTemporal\", \"password\": \"pasTemporal\", \"id\": \"idTemporal\", \"fecha\": \"feTemporal\"}]', response.data)")   
            self.assertIn(b'[{"correo": "coTemporal", "password": "pasTemporal", "id": "idTemporal", "fecha": "feTemporal"}]', response.data)
    

if __name__ == '__main__':
    unittest.main()