import re
import boto3
import logging
from botocore.exceptions import ClientError
from boto3.dynamodb.conditions import Key
from flask import Flask, request
from waitress import serve
from flask_mail import Mail,Message
import json
import logging
import creds
import random
import base64
import tempfile
import uuid
from twilio.rest import Client

app = Flask(__name__)
app.config['MAIL_SERVER'] = 'smtp.gmail.com'
app.config['MAIL_PORT'] = 587
app.config['MAIL_USE_TLS'] = True
app.config['MAIL_USERNAME'] = 'aydriveservice@gmail.com'
app.config['MAIL_PASSWORD'] = 'passwordaydrive123'
mail = Mail(app) 
account_sid='AC1f3788e2a832785ca74f9771c262d8b4'
auth_token='44eefdf6c0b20d7355bab8871ceccdaa'

client = Client(account_sid,auth_token)

@app.route('/')
def hello_world():
    return 'usuarios'

@app.route('/ddb/person/save', methods = ['POST'])
def ddb_save():
    if request.method == 'POST':
        content = request.get_json()
        id = content['id']
        correo = content['correo']
        password= content['password']
        fecha = content['fecha']
        celular = content['celular']
        dynamodb_client = boto3.client(
            'dynamodb',
            aws_access_key_id=creds.dynamodb['access_key_id'],
            aws_secret_access_key=creds.dynamodb['secret_access_key'],
            region_name=creds.dynamodb['region'],
        )

        try:
            response = dynamodb_client.put_item(
                TableName='usuario',
                Item={
                    'id': {'S': id},
                    'correo': {'S': correo},
                    'password': {'S': password},
                    'fecha': {'S':fecha},
                    'aut':{'S':"no"},
                    'celular': {'S':celular}
                },
            )
            logging.info(response)
            x="{ \"RESPUESTA\": \"EXITOSO/\"}"
            return json.loads(x)
        except ClientError as e:
            logging.error(e)
            x="{ \"RESPUESTA\": \"FALLO/\"}"
            return json.loads(x)

@app.route('/ddb/usuario/update', methods = ['PUT'])
def ddb_update():
    if request.method == 'PUT':
        content = request.get_json()
        idt = content['id']
        correo = content['correo']
        password = content['password']
        fecha = content['fecha']


        dynamodb_client = boto3.client(
            'dynamodb',
            aws_access_key_id=creds.dynamodb['access_key_id'],
            aws_secret_access_key=creds.dynamodb['secret_access_key'],
            region_name=creds.dynamodb['region'],
        )

        try:
            response = dynamodb_client.update_item(
                TableName='usuario',
                Key={'id': {'S': idt}},
                UpdateExpression='SET correo = :val1 , password= :val3 , fecha= :val4',
                ConditionExpression='id = :val2',
                ExpressionAttributeValues={
                    ':val1': {'S': correo},
                    ':val2': {'S': idt},
                    ':val3': {'S': password},
                    ':val4': {'S': fecha}
                },
                ReturnValues='UPDATED_NEW',
            )
            logging.info(response)
            x="{ \"RESPUESTA\": \"EXITOSO/\"}"
            return json.loads(x)
        except ClientError as e:
            logging.error(e)
            x="{ \"RESPUESTA\": \"FALLO/\"}"
            return json.loads(x)

@app.route('/ddb/usuario/updateAuth', methods = ['PUT'])
def ddb_updateAuth():
    if request.method == 'PUT':
        content = request.get_json()
        idt = content['id']
        authh=content['auth']

        dynamodb_client = boto3.client(
            'dynamodb',
            aws_access_key_id=creds.dynamodb['access_key_id'],
            aws_secret_access_key=creds.dynamodb['secret_access_key'],
            region_name=creds.dynamodb['region'],
        )

        try:
            response = dynamodb_client.update_item(
                TableName='usuario',
                Key={'id': {'S': idt}},
                UpdateExpression='SET aut = :val1',
                ConditionExpression='id = :val2',
                ExpressionAttributeValues={
                    ':val1': {'S': authh},
                    ':val2': {'S': idt}
                },
                ReturnValues='UPDATED_NEW',
            )
            logging.info(response)
            x="{ \"RESPUESTA\": \"EXITOSO/\"}"
            return json.loads(x)
        except ClientError as e:
            logging.error(e)
            x="{ \"RESPUESTA\": \"FALLO/\"}"
            return json.loads(x)


@app.route('/ddb/usuario/delete', methods = ['POST'])
def ddb_delete():
    if request.method == 'POST':
        content = request.get_json()
        idt = content['id']


        dynamodb_client = boto3.client(
            'dynamodb',
            aws_access_key_id=creds.dynamodb['access_key_id'],
            aws_secret_access_key=creds.dynamodb['secret_access_key'],
            region_name=creds.dynamodb['region'],
        )
        try:
            response = dynamodb_client.delete_item(
                TableName='usuario',
                Key={'id': {'S': idt}},
                ReturnValues='ALL_OLD',
            )
            logging.info(response)
            x="{ \"RESPUESTA\": \"EXITOSO/\"}"
            return json.loads(x)
        except ClientError as e:
            logging.error(e)
            x="{ \"RESPUESTA\": \"FALLO/\"}"
            return json.loads(x)

@app.route('/ddb/person/login', methods = ['POST'])
def ddb_login():
    if request.method == 'POST':
        content = request.get_json()
        idt = str(content['id'])
        pwd = str(content['password'])


        dynamodb = boto3.resource('dynamodb',
        aws_access_key_id=creds.dynamodb['access_key_id'],
        aws_secret_access_key=creds.dynamodb['secret_access_key'],
        region_name=creds.dynamodb['region'],)

        table = dynamodb.Table('usuario')
        response = table.query(
            KeyConditionExpression=Key('id').eq(idt)
        )
        datos=response['Items']
        for data in datos:
            if data['password']==pwd:
                if data['aut']=="si":
                    x="{ \"RESPUESTA\": \"EXITOSO/\",\"auth\": \"true\" }"
                else:
                    x="{ \"RESPUESTA\": \"EXITOSO/\"}"
                return json.loads(x)
        x="{ \"RESPUESTA\": \"FALLO/\"}"
        return json.loads(x)
   
@app.route('/ddb/usuario/Autenticacion', methods = ['POST'])
def ddb_Auth():
    if request.method == 'POST':
        content = request.get_json()
        idt = str(content['id'])
        ttoken = str(content['token'])
        fcel=str(content['cel'])
        fcorreo=str(content['correo'])

        dynamodb = boto3.resource('dynamodb',
        aws_access_key_id=creds.dynamodb['access_key_id'],
        aws_secret_access_key=creds.dynamodb['secret_access_key'],
        region_name=creds.dynamodb['region'],)

        table = dynamodb.Table('usuario')
        response = table.query(
            KeyConditionExpression=Key('id').eq(idt)
        )
        datos=response['Items']
        correo=''
        celular=''
        for data in datos:
            correo=data['correo']
            celular=data['celular']
        res=False
        if fcorreo=="si":
            res=send_email(correo,ttoken)
        if fcel=="si":
            res=send_text(celular,ttoken)
        if(res):
            x="{ \"RESPUESTA\": \"EXITOSO/\"}"
            return json.loads(x)
        x="{ \"RESPUESTA\": \"FALLO/\"}"
        return json.loads(x)


def send_email(correo,ttoken):
    msg = Message("Token de Autorizacion",sender="AyDrive.com",body="Ingrese este token para completar el inicio de sesión: "+ttoken,recipients=[correo])
    mail.send (msg)
    print(correo)
    return True
def send_text(cel,ttoken):
    message = client.messages \
    .create(
         body='Ingrese este token para iniciar sesion: '+ttoken,
         from_='+14243733543',
         to=cel
     )
    return True


@app.route('/sendcorreo', methods = ['POST'])
def correo():
    if request.method == 'POST':
        content = request.get_json()
        asunto = str(content['asunto'])
        correo = str(content['correo'])
        cuerpo = str(content['cuerpo'])
        msg = Message(asunto,sender="AyDrive.com",body=cuerpo,recipients=[correo])
        mail.send (msg)
        print(correo)
        x="{ \"RESPUESTA\": \"EXITOSO/\"}"
        return json.loads(x)

@app.route('/ddb/person/perfil', methods = ['POST'])
def ddb_perfil():
    if request.method == 'POST':
        content = request.get_json()
        idt = str(content['id'])

        dynamodb = boto3.resource('dynamodb',
        aws_access_key_id=creds.dynamodb['access_key_id'],
        aws_secret_access_key=creds.dynamodb['secret_access_key'],
        region_name=creds.dynamodb['region'],)

        table = dynamodb.Table('usuario')
        response = table.query(
            KeyConditionExpression=Key('id').eq(idt)
        )
        return json.dumps(response['Items'])


@app.route('/ddb/carpeta/save', methods = ['POST'])
def ddb_saveCarpeta():
    if request.method == 'POST':
        content = request.get_json()
        nombre = content['nombre']
        fecha = content['fecha']
        cantidad= content['cantidad']
        propietario= content['propietario']
        numeror=str(random.randint(0,500))
        dynamodb_client = boto3.client(
            'dynamodb',
            aws_access_key_id=creds.dynamodb['access_key_id'],
            aws_secret_access_key=creds.dynamodb['secret_access_key'],
            region_name=creds.dynamodb['region'],
        )

        try:
            response = dynamodb_client.put_item(
                TableName='carpetas',
                Item={
                    'codigo':{'S':numeror},
                    'nombre': {'S': nombre},
                    'fecha': {'S': fecha},
                    'cantidad': {'S': cantidad},
                    'propietario': {'S': propietario}
                },
            )
            logging.info(response)
            x = "{ \"RESPUESTA\": \"EXITOSO/\"}"
            return json.loads(x)
        except ClientError as e:
            logging.error(e)
            x = "{ \"RESPUESTA\": \"FALLO/\"}"
            return json.loads(x) 

@app.route('/ddb/archivo/save', methods = ['POST'])
def ddb_saveArchivo():
    if request.method == 'POST':
        content = request.get_json()
        nombre = content['nombre']
        fecha = content['fecha']
        carpeta= content['carpeta']
        propietario= content['propietario']
        link = content['link']
        extension=content['extension']
        numeror=str(random.randint(0,500))
        dynamodb_client = boto3.client(
            'dynamodb',
            aws_access_key_id=creds.dynamodb['access_key_id'],
            aws_secret_access_key=creds.dynamodb['secret_access_key'],
            region_name=creds.dynamodb['region'],
        )

        try:
            response = dynamodb_client.put_item(
                TableName='archivos',
                Item={
                    'codigo':{'S':numeror},
                    'nombre': {'S': nombre},
                    'fecha': {'S': fecha},
                    'carpeta': {'S': carpeta},
                    'propietario': {'S': propietario},
                    'link':{'S': link},
                    'extension':{'S': extension}
                },
            )
            logging.info(response)
            x = "{ \"RESPUESTA\": \"EXITOSO/\"}"
            return json.loads(x) 
        except ClientError as e:
            logging.error(e)
            x = "{ \"RESPUESTA\": \"FALLO/\"}"
            return json.loads(x) 

@app.route('/ddb/carpeta/update', methods = ['PUT'])
def ddb_updateCarpeta():
    if request.method == 'PUT':
        content = request.get_json()
        nombre = content['nombre']
        cantidad = content['cantidad']
        codigo = str(content['codigo'])
        username = str(content['propietario'])


        dynamodb_client = boto3.client(
            'dynamodb',
            aws_access_key_id=creds.dynamodb['access_key_id'],
            aws_secret_access_key=creds.dynamodb['secret_access_key'],
            region_name=creds.dynamodb['region']
        )

        try:
            response = dynamodb_client.update_item(
                TableName='carpetas',
                Key={'codigo': {'S': codigo},
                    'propietario':{'S':username}
                    },
                UpdateExpression='SET nombre= :val1, cantidad= :val2',
                ConditionExpression='codigo = :val3',
                ExpressionAttributeValues={
                    ':val1': {'S': nombre},
                    ':val2': {'S': cantidad},
                    ':val3': {'S':codigo}
                },
                ReturnValues='UPDATED_NEW'
            )
            logging.info(response)
            x = "{ \"RESPUESTA\": \"EXITOSO/\"}"
            return json.loads(x)
        except ClientError as e:
            logging.error(e)
            x = "{ \"RESPUESTA\": \"FALLO/\"}"
            return json.loads(x) 

@app.route('/ddb/archivo/update', methods = ['PUT'])
def ddb_updateArchivo():
    if request.method == 'PUT':
        content = request.get_json()
        nombre = content['nombre']
        link = content['link']
        codigo=str(content['codigo'])
        carpeta=content['carpeta']
        username = str(content['propietario'])


        dynamodb_client = boto3.client(
            'dynamodb',
            aws_access_key_id=creds.dynamodb['access_key_id'],
            aws_secret_access_key=creds.dynamodb['secret_access_key'],
            region_name=creds.dynamodb['region']
        )

        try:
            response = dynamodb_client.update_item(
                TableName='archivos',
                Key={'codigo': {'S': codigo},
                    'propietario':{'S':username}
                    },
                UpdateExpression='SET nombre= :val1, link= :val2, carpeta= :val4',
                ConditionExpression='codigo = :val3',
                ExpressionAttributeValues={
                    ':val1': {'S': nombre},
                    ':val2': {'S': link},
                    ':val3': {'S': codigo},
                    ':val4': {'S': carpeta}
                },
                ReturnValues='UPDATED_NEW'
            )
            logging.info(response)
            x = "{ \"RESPUESTA\": \"EXITOSO/\"}"
            return json.loads(x)
        except ClientError as e:
            logging.error(e)
            x = "{ \"RESPUESTA\": \"FALLO/\"}"
            return json.loads(x) 

@app.route('/ddb/carpeta/delete', methods = ['POST'])
def ddb_deleteCarpeta():
    if request.method == 'POST':
        content = request.get_json()
        codigo = str(content['codigo'])
        username = str(content['propietario'])


        dynamodb_client = boto3.client(
            'dynamodb',
            aws_access_key_id=creds.dynamodb['access_key_id'],
            aws_secret_access_key=creds.dynamodb['secret_access_key'],
            region_name=creds.dynamodb['region']
        )

        try:
            response = dynamodb_client.delete_item(
                TableName='carpetas',
                Key={'codigo': {'S': codigo},
                     'propietario':{'S':username}
                    }
            )
            logging.info(response)
            x = "{ \"RESPUESTA\": \"EXITOSO/\"}"
            return json.loads(x)
        except ClientError as e:
            logging.error(e)
            x = "{ \"RESPUESTA\": \"FALLO/\"}"
            return json.loads(x) 

@app.route('/ddb/archivo/delete', methods = ['POST'])
def ddb_deleteArchivo():
    if request.method == 'POST':
        content = request.get_json()
        codigo = str(content['codigo'])
        username = str(content['propietario'])

        dynamodb_client = boto3.client(
            'dynamodb',
            aws_access_key_id=creds.dynamodb['access_key_id'],
            aws_secret_access_key=creds.dynamodb['secret_access_key'],
            region_name=creds.dynamodb['region']
        )

        try:
            response = dynamodb_client.delete_item(
                TableName='archivos',
                Key={'codigo': {'S': codigo},
                     'propietario':{'S':username}
                },
                ReturnValues='ALL_OLD'
            )
            logging.info(response)
            x = "{ \"RESPUESTA\": \"EXITOSO/\"}"
            return json.loads(x)
        except ClientError as e:
            logging.error(e)
            x = "{ \"RESPUESTA\": \"FALLO/\"}"
            return json.loads(x) 

@app.route('/ddb/archivo/ver', methods = ['POST'])
def ddb_verArchivo():
    if request.method == 'POST':
        content = request.get_json()
        codigo = str(content['codigo'])

        dynamodb = boto3.resource('dynamodb',
        aws_access_key_id=creds.dynamodb['access_key_id'],
        aws_secret_access_key=creds.dynamodb['secret_access_key'],
        region_name=creds.dynamodb['region'],)

        table = dynamodb.Table('archivos')
        response = table.query(
            KeyConditionExpression=Key('codigo').eq(codigo)
        )
        return json.dumps(response['Items'])

@app.route('/ddb/carpeta/ver', methods = ['POST'])
def ddb_verCarpeta():
    if request.method == 'POST':
        content = request.get_json()
        codigo = str(content['codigo'])

        dynamodb = boto3.resource('dynamodb',
        aws_access_key_id=creds.dynamodb['access_key_id'],
        aws_secret_access_key=creds.dynamodb['secret_access_key'],
        region_name=creds.dynamodb['region'],)

        table = dynamodb.Table('carpetas')
        response = table.query(
            KeyConditionExpression=Key('codigo').eq(codigo)
        )
        return json.dumps(response['Items'])


@app.route('/ddb/archivo/all', methods = ['POST'])
def ddb_verArchivos():
    if request.method == 'POST':
        content = request.get_json()
        codigo = str(content['propietario'])

        dynamodb = boto3.resource('dynamodb',
        aws_access_key_id=creds.dynamodb['access_key_id'],
        aws_secret_access_key=creds.dynamodb['secret_access_key'],
        region_name=creds.dynamodb['region'],)

        table = dynamodb.Table('archivos')
        response = table.scan(
            FilterExpression=Key('propietario').eq(codigo)
        )
        print(response['Items'])
        return json.dumps(response['Items'])

@app.route('/ddb/carpeta/all', methods = ['POST'])
def ddb_verCarpetas():
    if request.method == 'POST':
        content = request.get_json()
        codigo = str(content['propietario'])

        dynamodb = boto3.resource('dynamodb',
        aws_access_key_id=creds.dynamodb['access_key_id'],
        aws_secret_access_key=creds.dynamodb['secret_access_key'],
        region_name=creds.dynamodb['region'],)

        table = dynamodb.Table('carpetas')
        response = table.scan(
            FilterExpression=Key('propietario').eq(codigo)
        )
        print(response['Items'])
        return json.dumps(response['Items'])

#-------------------------------------------------ARCHIVO.PY--------------------------------#

#CARGA DE UNA IMAGEN ESTA NO SE PUBLICA NO ES DE ESTE PROYECTO
@app.route('/s3/upload', methods = ['POST'])
def s3_upload():
    if request.method == 'POST':
        content = request.get_json()
        name = content['name']
        ext = content['ext']
        b64_parts = content['base64'].split(',')
        image_64_encode_str = len(b64_parts) ==2 and b64_parts[1] or b64_parts[0]

        s3_client = boto3.client(
            's3',
            aws_access_key_id=creds.s3['access_key_id'],
            aws_secret_access_key=creds.s3['secret_access_key'],
        )

        BUCKET_NAME = 'bucket-analisis2'
        FOLDER_NAME = 'images'
        file_name = '%s-%s.%s' % (name, uuid.uuid4(), ext)
        file_path = '%s/%s' % (FOLDER_NAME, file_name)
        image_64_encode = base64.b64decode((image_64_encode_str))
        f = tempfile.TemporaryFile()
        f.write(image_64_encode)
        f.seek(0)

        try:
            response = s3_client.put_object(Body=f, Bucket=BUCKET_NAME, Key=file_path, ACL='public-read')
            logging.info(response)
            return response
        except ClientError as e:
            logging.error(e)
            return e.response

#SUBIR ARCHIVOS el usuario root podra subir archovps YA12
@app.route('/s3/subirarchivo', methods = ['POST'])
def s3_subirarchivos():
    if request.method == 'POST':
        content = request.get_json()
        ext = content['ext']  #EXTENCION DEL ARCHIVO
        name = content['name']  #NOMBRE DEL ARCHIVO
        destino = content['destino'] # DIRECCION del bucket con slash pero sin el ultimo slash
        #origen = content['origen'] # DIRECCION en la pc con extencion
        b64_parts = content['base64'].split(',') #ARCHIVO EN BASE 64
        image_64_encode_str = len(b64_parts) ==2 and b64_parts[1] or b64_parts[0]


        s3_client = boto3.client(
            's3',
            aws_access_key_id=creds.s3['access_key_id'],
            aws_secret_access_key=creds.s3['secret_access_key'],
        )

        BUCKET_NAME = 'bucket-analisis2'
        file_name = '%s.%s' % (name,  ext)
        file_path = '%s/%s' % (destino, file_name)
        image_64_encode = base64.b64decode((image_64_encode_str))
        f = tempfile.TemporaryFile()
        f.write(image_64_encode)
        f.seek(0)

        try:
            #s3_client.upload_file(Filename=origen, Bucket=BUCKET_NAME, Key=file_path, ExtraArgs={'ACL':'public-read'})
            #s3_client.upload_file(origen, BUCKET_NAME, destino)
            response = s3_client.put_object(Body=f, Bucket=BUCKET_NAME, Key=file_path, ACL='public-read')            
            x = "{ \"RESPUESTA\": \"EXITOSO/\"}"
            return json.loads(x) 
        except FileNotFoundError:
            x = "{ \"RESPUESTA\": \"FALLO/\"}"
            return json.loads(x) 
        x = "{ \"name\": \"FALLO/\"}"
        return json.loads(x) 


#VER ARCHIVOS CARGADOS YA12
@app.route('/s3/verarchivos', methods = ['POST'])
def s3_verarchivos():
    if request.method == 'POST':
        content = request.get_json()
        name = content['name']  #Direccion de la carpeta a ver con ultimo slash
        

        s3_client = boto3.resource(
            's3',
            aws_access_key_id=creds.s3['access_key_id'],
            aws_secret_access_key=creds.s3['secret_access_key'],
        )

        my_bucket = s3_client.Bucket('bucket-analisis2')
        #BUCKET_NAME = 'bucket-analisis2'
        concatenacion = "{"
        contador=0
        for object_summary in my_bucket.objects.filter(Prefix=name):
           contador= contador+1
           concatenacion =  concatenacion + "\"name"+ str(contador)+"\": \""  + str(object_summary.key) + '\",'
        final_str=concatenacion[:-1] 
        final_str = final_str + "}"
        print(final_str)
        return json.loads(final_str) 
  

#Obtenemos las direcciones ip para validar las ubicaciones de los archivos YA12
@app.route('/s3/obtenerURL', methods = ['POST'])
def s3_obtenerURL():
    if request.method == 'POST':
        content = request.get_json()
        name = content['name']  #nombre del archivo con extension del archivo
        dire = content['dire']  #direccion donde esta el archivo con slash Cesar/
        

        s3_client = boto3.resource(
            's3',
            aws_access_key_id=creds.s3['access_key_id'],
            aws_secret_access_key=creds.s3['secret_access_key'],
        )

        my_bucket = s3_client.Bucket('bucket-analisis2')
       #https://bucket-analisis2.s3.us-east-2.amazonaws.com/Cesar/ejempkkloperrp1.PDF
        x = "{ \"Direccion\": \""+'https://bucket-analisis2.s3.us-east-2.amazonaws.com/'+  dire    +   name  +"\"}"
        #x = "{ \"name\": \"Cesar/\"}"
        return json.loads(x) 
        #return 'https://bucket-analisis2.s3.us-east-2.amazonaws.com/'+  dire    +   name



#CREAR ARCHIVOS SIN REPETIR YA12
@app.route('/s3/crearnuevoarchivosinrepetir', methods = ['POST'])
def s3_crearnuevoarchivosinrepetir():
   
    if request.method == 'POST':
        bandera = True;
        content = request.get_json()
        #VNOMBRE DEL ARCHIVO
        name = content['name'] #ID = CESAR nombre del archivo
        ext = content['ext']  #EXTENCION DEL ARCHIVO
        #DIRECCION DONDE SE VA ALMACENAR E IR A BUSCAR
        name2 = content['name2'] #ID = Nombre de la carpeta  donde va ir a ver si existe Cesar/ tiene que llevar slash
        #DIRECCION EN LA PC
        #origen = content['origen'] # DIRECCION en la pc
        b64_parts = content['base64'].split(',')
        image_64_encode_str = len(b64_parts) ==2 and b64_parts[1] or b64_parts[0]
        

        s3_client = boto3.client(
            's3',
            aws_access_key_id=creds.s3['access_key_id'],
            aws_secret_access_key=creds.s3['secret_access_key'],
        )
        
        s3_client2 = boto3.resource(
            's3',
            aws_access_key_id=creds.s3['access_key_id'],
            aws_secret_access_key=creds.s3['secret_access_key'],
        )

        BUCKET_NAME = 'bucket-analisis2'
        directory_name = name  #it's name of your folders
        #UNION DE TODO
        file_name = '%s.%s' % (name,  ext)
        file_path = '%s%s' % (name2, file_name) #Almacenamiento de la dierecion del archivo
        image_64_encode = base64.b64decode((image_64_encode_str))
        f = tempfile.TemporaryFile()
        f.write(image_64_encode)
        f.seek(0)


        #VALIDANDO QUE EXISTA LA CARPETA
        my_bucket = s3_client2.Bucket('bucket-analisis2')
        for object_summary in my_bucket.objects.filter(Prefix=name2):
            #print(object_summary.key)
            temporal = object_summary.key
            separador = "/"
            separado = temporal.split(separador)
            print(separado[1])
            if separado[1]  == file_name:
                print("YA EXISTE UN ARCHIVO CON ESTE NOMBRE")
                bandera=False
                break;
            else:
                print("NO EXISTE UN ARCHIVO CON ESTE NOMBRE DEBE CREARSE")
                bandera=True
        
        BUCKET_NAME = 'bucket-analisis2'
        if bandera:
            try:
                response = s3_client.put_object(Body=f, Bucket=BUCKET_NAME, Key=file_path, ACL='public-read')
                print(response)
                #s3_client.upload_file(Filename=origen, Bucket=BUCKET_NAME, Key=file_path, ExtraArgs={'ACL':'public-read'})         
                x = "{ \"RESPUESTA\": \"SI SE CREO/\"}"
                return json.loads(x) 
            except ClientError as e:
               # logging.error(e)
                x = "{ \"RESPUESTA\": \"NO SE CREO/\"}"
                return json.loads(x) 
                
    x = "{ \"RESPUESTA\": \"NO SE CREO/\"}"
    return json.loads(x) 



#ELIMINAR ARCHIVOS YA2 QUEDA PENDIENTE CARPETAS--------------------
@app.route('/s3/eliminararchivo', methods = ['POST'])
def s3_eliminararchivo():
    if request.method == 'POST':
        content = request.get_json()
        ext = content['ext']  #EXTENCION DEL ARCHIVO con punto
        name = content['name']  #NOMBRE DEL ARCHIVO SI ES CARPETA AGREGAR SLASH AL FINAL 
        destino = content['destino'] # DIRECCION del bucket sin slash final
        

        s3_client = boto3.client(
            's3',
            aws_access_key_id=creds.s3['access_key_id'],
            aws_secret_access_key=creds.s3['secret_access_key'],
        )

        BUCKET_NAME = 'bucket-analisis2'
        if ext != "":
            file_name = '%s%s' % (name,  ext)
            file_path = '%s/%s' % (destino, file_name)
        else:
            file_name = '%s%s' % (name,  ext)
            file_path = '%s/%s' % (destino, file_name)

        try:
           # s3_client.upload_file(Filename=origen, Bucket=BUCKET_NAME, Key=file_path, ExtraArgs={'ACL':'public-read'})
            s3_client.delete_object(Bucket=BUCKET_NAME, Key=file_path)
            #s3_client.upload_file(origen, BUCKET_NAME, destino)
            print("Eliminado exitosamente " + file_path)
            x = "{ \"RESPUESTA\": \"SI ELIMINO/\"}"
            return json.loads(x) 
        except FileNotFoundError:
            x = "{ \"RESPUESTA\": \"NO ELIMIINO/\"}"
            return json.loads(x) 
        x = "{ \"RESPUESTA\": \"NO SE ELIMINO/\"}"
        return json.loads(x) 



#MOVER LOS ARCHIVOS YA12
@app.route('/s3/moverlosarchivos', methods = ['POST'])
def s3_moverlosarchivos():
    if request.method == 'POST':
        content = request.get_json()
        direccionvieja = content['direccionvieja']  #nombre del archivo con extencion ubicacion exacta
        direccionnueva = content['direccionnueva']  #nombre del nuevo archivo con extencion ubicacion exactao
      

        

        s3_client = boto3.resource(
            's3',
            aws_access_key_id=creds.s3['access_key_id'],
            aws_secret_access_key=creds.s3['secret_access_key'],
        )

        my_bucket = s3_client.Bucket('bucket-analisis2')
        # SI SE LOGRO ACCEDER Y COPIAR TODO
        #s3_client.Object('bucket-analisis2', 'Cesar/holamundo.PDF').copy_from(CopySource='Cesar/ejempkkloperrp2.PDF',ACL='public-read')
        copy_source = {
        'Bucket': 'bucket-analisis2',  
        'Key': direccionvieja
        }
        s3_client.meta.client.copy(copy_source, 'bucket-analisis2',direccionnueva,ExtraArgs={'ACL':'public-read'})
        # Delete the former object A
        s3_client.Object('bucket-analisis2', direccionvieja).delete()
        
        x = "{ \"RESPUESTA\": \"SI SE MOVIO DE LUGAR/\"}"
        return json.loads(x) 


#==================================================
#==================================================
#================NUEVOS SERCICIOS==================
#==================================================
#==================================================


#MOVER LOS ARCHIVOS YA12
@app.route('/s3/moverpapelera', methods = ['POST'])
def s3_moverlosarchivosdelapapelera():
    if request.method == 'POST':
        content = request.get_json()
        direccionvieja = content['direccionvieja']  #nombre del archivo con extencion ubicacion exacta
        direccionnueva = content['direccionnueva']  #nombre del nuevo archivo con extencion ubicacion exactao
      

        

        s3_client = boto3.resource(
            's3',
            aws_access_key_id=creds.s3['access_key_id'],
            aws_secret_access_key=creds.s3['secret_access_key'],
        )

        my_bucket = s3_client.Bucket('bucket-analisis2')
        # SI SE LOGRO ACCEDER Y COPIAR TODO
        #s3_client.Object('bucket-analisis2', 'Cesar/holamundo.PDF').copy_from(CopySource='Cesar/ejempkkloperrp2.PDF',ACL='public-read')
        copy_source = {
        'Bucket': 'bucket-analisis2',  
        'Key': direccionvieja
        }
        s3_client.meta.client.copy(copy_source, 'bucket-analisis2',direccionnueva,ExtraArgs={'ACL':'public-read'})
        # Delete the former object A
        s3_client.Object('bucket-analisis2', direccionvieja).delete()
        
        x = "{ \"RESPUESTA\": \"SI SE MOVIO DE LUGAR/\"}"
        return json.loads(x) 



@app.route('/s3/eliminararchivopapelera', methods = ['POST'])
def s3_eliminararchivopapelera():
    if request.method == 'POST':
        content = request.get_json()
        ext = content['ext']  #EXTENCION DEL ARCHIVO con punto
        name = content['name']  #NOMBRE DEL ARCHIVO SI ES CARPETA AGREGAR SLASH AL FINAL 
        destino = content['destino'] # DIRECCION del bucket sin slash final
        

        s3_client = boto3.client(
            's3',
            aws_access_key_id=creds.s3['access_key_id'],
            aws_secret_access_key=creds.s3['secret_access_key'],
        )

        BUCKET_NAME = 'bucket-analisis2'
        if ext != "":
            file_name = '%s%s' % (name,  ext)
            file_path = '%s/%s' % (destino, file_name)
        else:
            file_name = '%s%s' % (name,  ext)
            file_path = '%s/%s' % (destino, file_name)

        try:
           # s3_client.upload_file(Filename=origen, Bucket=BUCKET_NAME, Key=file_path, ExtraArgs={'ACL':'public-read'})
            s3_client.delete_object(Bucket=BUCKET_NAME, Key=file_path)
            #s3_client.upload_file(origen, BUCKET_NAME, destino)
            print("Eliminado exitosamente " + file_path)
            x = "{ \"RESPUESTA\": \"SI ELIMINO/\"}"
            return json.loads(x) 
        except FileNotFoundError:
            x = "{ \"RESPUESTA\": \"NO ELIMIINO/\"}"
            return json.loads(x) 
        x = "{ \"RESPUESTA\": \"NO SE ELIMINO/\"}"
        return json.loads(x) 


#--------------------------------CARPETAS.PY----------------------------------------------------#

#CREAR CARPETA DE CADA USUARIO ROOT al momento de login debe crear una carpeta root por usuario YA12
@app.route('/s3/carpetaroot', methods = ['POST'])
def s3_crearcarpeta():
    if request.method == 'POST':
        content = request.get_json()
        name = content['name']
        

        s3_client = boto3.client(
            's3',
            aws_access_key_id=creds.s3['access_key_id'],
            aws_secret_access_key=creds.s3['secret_access_key'],
        )

        BUCKET_NAME = 'bucket-analisis2'
        FOLDER_NAME = 'images'
        directory_name = name #it's name of your folders

        try:
            response = s3_client.put_object(Bucket=BUCKET_NAME, Key=(directory_name+'/'))
            logging.info(response)
            x = "{ \"RESPUESTA\": \"SE CREO CARPETA ROOT/\"}"
            return json.loads(x) 
        except ClientError as e:
            logging.error(e)
            x = "{ \"RESPUESTA\": \"ERROR NO CREO CARPETA ROOT/\"}"
            return json.loads(x) 

#CREAR CARPETAS EN LA CARPETA ROOT podra tener acceso a su carpeta root y crear dentro de ella mas carpetas YA12
@app.route('/s3/crearcarpetasenlaroot', methods = ['POST'])
def s3_crearcarpetasenroot():
    if request.method == 'POST':
        content = request.get_json()
        name = content['name'] #ID = CESAR
        carpetas = content['carpetas'] # DIRECCION carpeta1/carpeta2/  = CESAR/carpeta1/carpeta2/ tiene que llevar slash al final 
        

        s3_client = boto3.client(
            's3',
            aws_access_key_id=creds.s3['access_key_id'],
            aws_secret_access_key=creds.s3['secret_access_key'],
        )

        BUCKET_NAME = 'bucket-analisis2'
        FOLDER_NAME = 'images'
        directory_name = name  #it's name of your folders

        try:
            response = s3_client.put_object(Bucket=BUCKET_NAME, Key=(directory_name+'/'+carpetas))
            x = "{ \"RESPUESTA\": \"SE CREO CARPETA EN LA CARPETA ROOT/\"}"
            return json.loads(x) 
        except ClientError as e:
            logging.error(e)
            x = "{ \"RESPUESTA\": \"ERROR NO SE CREO CARPETA EN LA CARPETA ROOT/\"}"
            return json.loads(x) 


#CREAR CARPETAS EN LA CARPETA ROOT SIN REPETIR YA12
@app.route('/s3/crearcarpetanueva', methods = ['POST'])
def s3_crearcarpetanueva():
   
    if request.method == 'POST':
        bandera = True;
        content = request.get_json()
        name = content['name'] #ID = SIN SLASH  nombre de la carpeta nueva Nombre de la carpeta donde va ir a ver si existe Cesar/ tiene que llevar slash
        name2 = content['name2'] #ID = CESAR/ nombre del root cons slash
       
        

        s3_client = boto3.client(
            's3',
            aws_access_key_id=creds.s3['access_key_id'],
            aws_secret_access_key=creds.s3['secret_access_key'],
        )
        
        s3_client2 = boto3.resource(
            's3',
            aws_access_key_id=creds.s3['access_key_id'],
            aws_secret_access_key=creds.s3['secret_access_key'],
        )

        BUCKET_NAME = 'bucket-analisis2'
        directory_name = name  #it's name of your folders

        #VALIDANDO QUE EXISTA LA CARPETA
        my_bucket = s3_client2.Bucket('bucket-analisis2')
        for object_summary in my_bucket.objects.filter(Prefix=name2):
            #print(object_summary.key)
            temporal = object_summary.key
            separador = "/"
            separado = temporal.split(separador)
            print(separado[1])
            if separado[1]  == name:
                print("YA EXISTE UNA CARPETA CON ESE NOMBRE NO DEBE CREAR")
                bandera=False
                break;
            else:
                print("NO EXISTE CARPETA CON ESE NOMBRE DEBE CREAR")
                bandera=True
        
        BUCKET_NAME = 'bucket-analisis2'
        if bandera:
            try:
                response = s3_client.put_object(Bucket=BUCKET_NAME, Key=(name2+''+name+'/'))
                logging.info(response)
                x = "{ \"RESPUESTA\": \"SI SE CREO/\"}"
                return json.loads(x) 
            except ClientError as e:
                logging.error(e)
                x = "{ \"RESPUESTA\": \"ERROR NO  SE CREO/\"}"
                return json.loads(x) 
                
    x = "{ \"RESPUESTA\": \"ERROR NO  SE CREO/\"}"
    return json.loads(x) 


#RENOMBRAR CARPETAS O ARCHIVOS YA12
@app.route('/s3/renamefileTODO', methods = ['POST'])
def s3_renamefileTODO():
    if request.method == 'POST':
        content = request.get_json()
        direccionviejonombre = content['direccionviejonombre']  #nombre del archivo con extencion ubicacion exacta
        direccionnuevonombre = content['direccionnuevonombre']  #nombre del nuevo archivo con extencion ubicacion exactao
      

        

        s3_client = boto3.resource(
            's3',
            aws_access_key_id=creds.s3['access_key_id'],
            aws_secret_access_key=creds.s3['secret_access_key'],
        )

        my_bucket = s3_client.Bucket('bucket-analisis2')
        # SI SE LOGRO ACCEDER Y COPIAR TODO
        #s3_client.Object('bucket-analisis2', 'Cesar/holamundo.PDF').copy_from(CopySource='Cesar/ejempkkloperrp2.PDF',ACL='public-read')
        copy_source = {
        'Bucket': 'bucket-analisis2',  
        'Key': direccionviejonombre
        }
        s3_client.meta.client.copy(copy_source, 'bucket-analisis2',direccionnuevonombre,ExtraArgs={'ACL':'public-read'})
        # Delete the former object A
        s3_client.Object('bucket-analisis2', direccionviejonombre).delete()
        
        x = "{ \"RESPUESTA\": \"SI SE RENOMBRO/\"}"
        return json.loads(x) 



#==================================================
#==================================================
#================NUEVOS SERCICIOS==================
#==================================================
#==================================================

#CREAR CARPETAS DE RECICLAJE EN LA CARPETA ROOT
@app.route('/s3/crearpapelera', methods = ['POST'])
def s3_crearcarpetapepelerea():
    if request.method == 'POST':
        content = request.get_json()
        name = content['name'] #ID = CESAR
       # carpetas = content['carpetas'] # DIRECCION carpeta1/carpeta2/  = CESAR/carpeta1/carpeta2/ tiene que llevar slash al final 
        

        s3_client = boto3.client(
            's3',
            aws_access_key_id=creds.s3['access_key_id'],
            aws_secret_access_key=creds.s3['secret_access_key'],
        )

        BUCKET_NAME = 'bucket-analisis2'
        directory_name = name  #it's name of your folders

        try:
            response = s3_client.put_object(Bucket=BUCKET_NAME, Key=(directory_name+'/'+'papelera/'))
            x = "{ \"RESPUESTA\": \"SE CREO CARPETA  DE PAPELERA EN LA CARPETA ROOT/\"}"
            return json.loads(x) 
        except ClientError as e:
            logging.error(e)
            x = "{ \"RESPUESTA\": \"ERROR NO SE CREO CARPETA DE PAPELERA EN LA CARPETA ROOT/\"}"
            return json.loads(x) 



if __name__ == '__main__':
    serve(app, host="0.0.0.0", port=3000)