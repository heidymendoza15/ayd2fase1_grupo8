from archivo import app
import unittest


class FlaskTestCase(unittest.TestCase):

  



    def testEliminararchovosdelapapelera(self):
        tester = app.test_client(self)
        response = tester.post('/s3/eliminararchivopapelera', json={        'ext': '.PDF', 'name': 'archivo2',  'destino': 'Cesar/papelera'  })
        print("Procesando prueba unitaria ......")
        print('tester.post(\'/s3/eliminararchivopapelera\', json={        \'ext\': \'.PDF\', \'name\': \'archivo2\',  \'destino\': \'Cesar/papelera\'  })')   
        print("EVALUANDO........")  
        print(" self.assertIn(b'{\"RESPUESTA\":\"SI ELIMINO\"}\', response.data)")     
        self.assertIn(b'{"RESPUESTA":"SI ELIMINO/"}\n', response.data)

     
    def testverarchivo(self):
        tester = app.test_client(self)
        response = tester.post('/s3/verarchivos', json={   'name': 'Cesar' })
        print("Procesando prueba unitaria ......")
        print('tester.post(\'/s3/verarchivos\', json={   \'name\': \'Cesar\' })')   
        print("EVALUANDO........") 
        print("json_data = response.get_json()")
        print("assert generate_response(name, json_data['resp'])")
        json_data = response.get_json()
        if(json_data != ""):
            tempo ="EXITO"
            self.assertIn("EXITO",tempo)
    
    def testobtenerURL(self):
        tester = app.test_client(self)
        response = tester.post('/s3/obtenerURL', json={   'name': 'Cesar', 'dire': 'dire'  })
        print("Procesando prueba unitaria ......")
        print('tester.post(\'/s3/obtenerURL\', json={   \'name\': \'Cesar\', \'dire\': \'dire\'  })')   
        print("EVALUANDO........") 
        print("json_data = response.get_json()")
        print("assert generate_response(dire, json_data['dire'])")
        json_data = response.get_json()
        if(json_data != ""):
            tempo ="EXITO"
            self.assertIn("EXITO",tempo)
         
    #def testcrearnuevoarchivosinrepetira(self):
       # tester = app.test_client(self)
        #response = tester.post('/s3/crearnuevoarchivosinrepetir', json={        'name': 'Cear', 'ext': '.PDF',  'name2': 'Cesar/', 'origen': 'C:/Users/cesa_/OneDrive/Escritorio/HOJA.PDF'  })
        #print("Procesando prueba unitaria ......")
       # print('tester.post(\'/s3/crearnuevoarchivosinrepetir\', json={        \'name\': \'Cear\', \'ext\': \'.PDF\',  \'name2\': \'Cesar/\', \'origen\': \'C:/Users/cesa_/OneDrive/Escritorio/HOJA.PDF\'  })')   
       # print("EVALUANDOssssssss........")  
       # print(response.data)
       # print(" self.assertIn(b'{\"RESPUESTA\":\"SI ELIMINO\"}\', response.data)")     
        #self.assertIn(b'{"RESPUESTA":"SI SE CREO/"}\n', response.data)


     
if __name__ == '__main__':
    unittest.main()


