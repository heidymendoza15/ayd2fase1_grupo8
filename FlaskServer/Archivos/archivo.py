import re
import boto3
import logging
import botocore
from botocore.exceptions import ClientError
from boto3.dynamodb.conditions import Key, Attr
from flask import Flask, request
from waitress import serve
import json
import base64
import tempfile
import uuid
import logging
import creds

app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'archivos'


#CARGA DE UNA IMAGEN ESTA NO SE PUBLICA NO ES DE ESTE PROYECTO
@app.route('/s3/upload', methods = ['POST'])
def s3_upload():
    if request.method == 'POST':
        content = request.get_json()
        name = content['name']
        ext = content['ext']
        b64_parts = content['base64'].split(',')
        image_64_encode_str = len(b64_parts) ==2 and b64_parts[1] or b64_parts[0]

        s3_client = boto3.client(
            's3',
            aws_access_key_id=creds.s3['access_key_id'],
            aws_secret_access_key=creds.s3['secret_access_key'],
        )

        BUCKET_NAME = 'bucket-analisis2'
        FOLDER_NAME = 'images'
        file_name = '%s-%s.%s' % (name, uuid.uuid4(), ext)
        file_path = '%s/%s' % (FOLDER_NAME, file_name)
        image_64_encode = base64.b64decode((image_64_encode_str))
        f = tempfile.TemporaryFile()
        f.write(image_64_encode)
        f.seek(0)

        try:
            response = s3_client.put_object(Body=f, Bucket=BUCKET_NAME, Key=file_path, ACL='public-read')
            logging.info(response)
            return response
        except ClientError as e:
            logging.error(e)
            return e.response



#SUBIR ARCHIVOS el usuario root podra subir archovps YA12
@app.route('/s3/subirarchivo', methods = ['POST'])
def s3_subirarchivos():
    if request.method == 'POST':
        content = request.get_json()
        ext = content['ext']  #EXTENCION DEL ARCHIVO
        name = content['name']  #NOMBRE DEL ARCHIVO
        destino = content['destino'] # DIRECCION del bucket con slash pero sin el ultimo slash
        #origen = content['origen'] # DIRECCION en la pc con extencion
        b64_parts = content['base64'].split(',') #ARCHIVO EN BASE 64
        image_64_encode_str = len(b64_parts) ==2 and b64_parts[1] or b64_parts[0]


        s3_client = boto3.client(
            's3',
            aws_access_key_id=creds.s3['access_key_id'],
            aws_secret_access_key=creds.s3['secret_access_key'],
        )

        BUCKET_NAME = 'bucket-analisis2'
        file_name = '%s.%s' % (name,  ext)
        file_path = '%s/%s' % (destino, file_name)
        image_64_encode = base64.b64decode((image_64_encode_str))
        f = tempfile.TemporaryFile()
        f.write(image_64_encode)
        f.seek(0)

        try:
            #s3_client.upload_file(Filename=origen, Bucket=BUCKET_NAME, Key=file_path, ExtraArgs={'ACL':'public-read'})
            #s3_client.upload_file(origen, BUCKET_NAME, destino)
            response = s3_client.put_object(Body=f, Bucket=BUCKET_NAME, Key=file_path, ACL='public-read')            
            x = "{ \"RESPUESTA\": \"EXITOSO/\"}"
            return json.loads(x) 
        except FileNotFoundError:
            x = "{ \"RESPUESTA\": \"FALLO/\"}"
            return json.loads(x) 
        x = "{ \"name\": \"FALLO/\"}"
        return json.loads(x) 


#VER ARCHIVOS CARGADOS YA12
@app.route('/s3/verarchivos', methods = ['POST'])
def s3_verarchivos():
    if request.method == 'POST':
        content = request.get_json()
        name = content['name']  #Direccion de la carpeta a ver con ultimo slash
        

        s3_client = boto3.resource(
            's3',
            aws_access_key_id=creds.s3['access_key_id'],
            aws_secret_access_key=creds.s3['secret_access_key'],
        )

        my_bucket = s3_client.Bucket('bucket-analisis2')
        #BUCKET_NAME = 'bucket-analisis2'
        concatenacion = "{"
        contador=0
        for object_summary in my_bucket.objects.filter(Prefix=name):
           contador= contador+1
           concatenacion =  concatenacion + "\"name"+ str(contador)+"\": \""  + str(object_summary.key) + '\",'
        final_str=concatenacion[:-1] 
        final_str = final_str + "}"
        print(final_str)
        return json.loads(final_str) 
  

#Obtenemos las direcciones ip para validar las ubicaciones de los archivos YA12
@app.route('/s3/obtenerURL', methods = ['POST'])
def s3_obtenerURL():
    if request.method == 'POST':
        content = request.get_json()
        name = content['name']  #nombre del archivo con extension del archivo
        dire = content['dire']  #direccion donde esta el archivo con slash Cesar/
        

        s3_client = boto3.resource(
            's3',
            aws_access_key_id=creds.s3['access_key_id'],
            aws_secret_access_key=creds.s3['secret_access_key'],
        )

        my_bucket = s3_client.Bucket('bucket-analisis2')
       #https://bucket-analisis2.s3.us-east-2.amazonaws.com/Cesar/ejempkkloperrp1.PDF
        x = "{ \"Direccion\": \""+'https://bucket-analisis2.s3.us-east-2.amazonaws.com/'+  dire    +   name  +"\"}"
        #x = "{ \"name\": \"Cesar/\"}"
        return json.loads(x) 
        #return 'https://bucket-analisis2.s3.us-east-2.amazonaws.com/'+  dire    +   name



#CREAR ARCHIVOS SIN REPETIR YA12
@app.route('/s3/crearnuevoarchivosinrepetir', methods = ['POST'])
def s3_crearnuevoarchivosinrepetir():
   
    if request.method == 'POST':
        bandera = True;
        content = request.get_json()
        #VNOMBRE DEL ARCHIVO
        name = content['name'] #ID = CESAR nombre del archivo
        ext = content['ext']  #EXTENCION DEL ARCHIVO
        #DIRECCION DONDE SE VA ALMACENAR E IR A BUSCAR
        name2 = content['name2'] #ID = Nombre de la carpeta  donde va ir a ver si existe Cesar/ tiene que llevar slash
        #DIRECCION EN LA PC
        #origen = content['origen'] # DIRECCION en la pc
        b64_parts = content['base64'].split(',')
        image_64_encode_str = len(b64_parts) ==2 and b64_parts[1] or b64_parts[0]
        

        s3_client = boto3.client(
            's3',
            aws_access_key_id=creds.s3['access_key_id'],
            aws_secret_access_key=creds.s3['secret_access_key'],
        )
        
        s3_client2 = boto3.resource(
            's3',
            aws_access_key_id=creds.s3['access_key_id'],
            aws_secret_access_key=creds.s3['secret_access_key'],
        )

        BUCKET_NAME = 'bucket-analisis2'
        directory_name = name  #it's name of your folders
        #UNION DE TODO
        file_name = '%s.%s' % (name,  ext)
        file_path = '%s%s' % (name2, file_name) #Almacenamiento de la dierecion del archivo
        image_64_encode = base64.b64decode((image_64_encode_str))
        f = tempfile.TemporaryFile()
        f.write(image_64_encode)
        f.seek(0)


        #VALIDANDO QUE EXISTA LA CARPETA
        my_bucket = s3_client2.Bucket('bucket-analisis2')
        for object_summary in my_bucket.objects.filter(Prefix=name2):
            #print(object_summary.key)
            temporal = object_summary.key
            separador = "/"
            separado = temporal.split(separador)
            print(separado[1])
            if separado[1]  == file_name:
                print("YA EXISTE UN ARCHIVO CON ESTE NOMBRE")
                bandera=False
                break;
            else:
                print("NO EXISTE UN ARCHIVO CON ESTE NOMBRE DEBE CREARSE")
                bandera=True
        
        BUCKET_NAME = 'bucket-analisis2'
        if bandera:
            try:
                response = s3_client.put_object(Body=f, Bucket=BUCKET_NAME, Key=file_path, ACL='public-read')
                print(response)
                #s3_client.upload_file(Filename=origen, Bucket=BUCKET_NAME, Key=file_path, ExtraArgs={'ACL':'public-read'})         
                x = "{ \"RESPUESTA\": \"SI SE CREO/\"}"
                return json.loads(x) 
            except ClientError as e:
               # logging.error(e)
                x = "{ \"RESPUESTA\": \"NO SE CREO/\"}"
                return json.loads(x) 
                
    x = "{ \"RESPUESTA\": \"NO SE CREO/\"}"
    return json.loads(x) 



#ELIMINAR ARCHIVOS YA2 QUEDA PENDIENTE CARPETAS--------------------
@app.route('/s3/eliminararchivo', methods = ['POST'])
def s3_eliminararchivo():
    if request.method == 'POST':
        content = request.get_json()
        ext = content['ext']  #EXTENCION DEL ARCHIVO con punto
        name = content['name']  #NOMBRE DEL ARCHIVO SI ES CARPETA AGREGAR SLASH AL FINAL 
        destino = content['destino'] # DIRECCION del bucket sin slash final
        

        s3_client = boto3.client(
            's3',
            aws_access_key_id=creds.s3['access_key_id'],
            aws_secret_access_key=creds.s3['secret_access_key'],
        )

        BUCKET_NAME = 'bucket-analisis2'
        if ext != "":
            file_name = '%s%s' % (name,  ext)
            file_path = '%s/%s' % (destino, file_name)
        else:
            file_name = '%s%s' % (name,  ext)
            file_path = '%s/%s' % (destino, file_name)

        try:
           # s3_client.upload_file(Filename=origen, Bucket=BUCKET_NAME, Key=file_path, ExtraArgs={'ACL':'public-read'})
            s3_client.delete_object(Bucket=BUCKET_NAME, Key=file_path)
            #s3_client.upload_file(origen, BUCKET_NAME, destino)
            print("Eliminado exitosamente " + file_path)
            x = "{ \"RESPUESTA\": \"SI ELIMINO/\"}"
            return json.loads(x) 
        except FileNotFoundError:
            x = "{ \"RESPUESTA\": \"NO ELIMIINO/\"}"
            return json.loads(x) 
        x = "{ \"RESPUESTA\": \"NO SE ELIMINO/\"}"
        return json.loads(x) 



#MOVER LOS ARCHIVOS YA12
@app.route('/s3/moverlosarchivos', methods = ['POST'])
def s3_moverlosarchivos():
    if request.method == 'POST':
        content = request.get_json()
        direccionvieja = content['direccionvieja']  #nombre del archivo con extencion ubicacion exacta
        direccionnueva = content['direccionnueva']  #nombre del nuevo archivo con extencion ubicacion exactao
      

        

        s3_client = boto3.resource(
            's3',
            aws_access_key_id=creds.s3['access_key_id'],
            aws_secret_access_key=creds.s3['secret_access_key'],
        )

        my_bucket = s3_client.Bucket('bucket-analisis2')
        # SI SE LOGRO ACCEDER Y COPIAR TODO
        #s3_client.Object('bucket-analisis2', 'Cesar/holamundo.PDF').copy_from(CopySource='Cesar/ejempkkloperrp2.PDF',ACL='public-read')
        copy_source = {
        'Bucket': 'bucket-analisis2',  
        'Key': direccionvieja
        }
        s3_client.meta.client.copy(copy_source, 'bucket-analisis2',direccionnueva,ExtraArgs={'ACL':'public-read'})
        # Delete the former object A
        s3_client.Object('bucket-analisis2', direccionvieja).delete()
        
        x = "{ \"RESPUESTA\": \"SI SE MOVIO DE LUGAR/\"}"
        return json.loads(x) 


#==================================================
#==================================================
#================NUEVOS SERCICIOS==================
#==================================================
#==================================================


#MOVER LOS ARCHIVOS YA12
@app.route('/s3/moverpapelera', methods = ['POST'])
def s3_moverlosarchivosdelapapelera():
    if request.method == 'POST':
        content = request.get_json()
        direccionvieja = content['direccionvieja']  #nombre del archivo con extencion ubicacion exacta
        direccionnueva = content['direccionnueva']  #nombre del nuevo archivo con extencion ubicacion exactao
      

        

        s3_client = boto3.resource(
            's3',
            aws_access_key_id=creds.s3['access_key_id'],
            aws_secret_access_key=creds.s3['secret_access_key'],
        )

        my_bucket = s3_client.Bucket('bucket-analisis2')
        # SI SE LOGRO ACCEDER Y COPIAR TODO
        #s3_client.Object('bucket-analisis2', 'Cesar/holamundo.PDF').copy_from(CopySource='Cesar/ejempkkloperrp2.PDF',ACL='public-read')
        copy_source = {
        'Bucket': 'bucket-analisis2',  
        'Key': direccionvieja
        }
        s3_client.meta.client.copy(copy_source, 'bucket-analisis2',direccionnueva,ExtraArgs={'ACL':'public-read'})
        # Delete the former object A
        s3_client.Object('bucket-analisis2', direccionvieja).delete()
        
        x = "{ \"RESPUESTA\": \"SI SE MOVIO DE LUGAR/\"}"
        return json.loads(x) 



@app.route('/s3/eliminararchivopapelera', methods = ['POST'])
def s3_eliminararchivopapelera():
    if request.method == 'POST':
        content = request.get_json()
        ext = content['ext']  #EXTENCION DEL ARCHIVO con punto
        name = content['name']  #NOMBRE DEL ARCHIVO SI ES CARPETA AGREGAR SLASH AL FINAL 
        destino = content['destino'] # DIRECCION del bucket sin slash final
        

        s3_client = boto3.client(
            's3',
            aws_access_key_id=creds.s3['access_key_id'],
            aws_secret_access_key=creds.s3['secret_access_key'],
        )

        BUCKET_NAME = 'bucket-analisis2'
        if ext != "":
            file_name = '%s%s' % (name,  ext)
            file_path = '%s/%s' % (destino, file_name)
        else:
            file_name = '%s%s' % (name,  ext)
            file_path = '%s/%s' % (destino, file_name)

        try:
           # s3_client.upload_file(Filename=origen, Bucket=BUCKET_NAME, Key=file_path, ExtraArgs={'ACL':'public-read'})
            s3_client.delete_object(Bucket=BUCKET_NAME, Key=file_path)
            #s3_client.upload_file(origen, BUCKET_NAME, destino)
            print("Eliminado exitosamente " + file_path)
            x = "{ \"RESPUESTA\": \"SI ELIMINO/\"}"
            return json.loads(x) 
        except FileNotFoundError:
            x = "{ \"RESPUESTA\": \"NO ELIMIINO/\"}"
            return json.loads(x) 
        x = "{ \"RESPUESTA\": \"NO SE ELIMINO/\"}"
        return json.loads(x) 

if __name__ == '__main__':
    serve(app, host="0.0.0.0", port=5000)