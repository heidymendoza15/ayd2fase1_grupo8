import React from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import Home from '../Pages/Home';
import Profile from '../Pages/Profile';
import Edit_Profile from '../Pages/Edit_Profile';
import Login from '../Components/Login';
import Register from '../Components/Register';
import Drive from '../Components/Drive';
import V_folder from '../Pages/V_folder';
import Papelera from '../Components/Papelera';
import Auth from '../Pages/Auth';

function Routes(){
    return(
        <BrowserRouter>
            <Switch>
                <Route exact path="/" component={Home}/>
                <Route exact path="/profile" component={Profile}/>
                <Route exact path="/login" component={Login}/>
                <Route exact path="/signup" component={Register}/>
                <Route exact path="/aydrive" component={Drive}/>
                <Route exact path="/edit_profile" component={Edit_Profile}/>
                <Route path="/folder/:name" component={V_folder}/>
                <Route exact path="/papelera" component={Papelera}/>
                <Route exact path="/login_token" component={Auth}/>
            </Switch>
        </BrowserRouter>
    );
}

export default Routes;
//<Route exact path="/folder" component={V_folder}/>