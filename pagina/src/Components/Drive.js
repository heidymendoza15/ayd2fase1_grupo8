import React, {useState, useEffect} from 'react'
import CreateNewFolderIcon from '@material-ui/icons/CreateNewFolder';
import FolderIcon from '@material-ui/icons/Folder';
import FolderSharedIcon from '@material-ui/icons/FolderShared';
import NoteAddIcon from '@material-ui/icons/NoteAdd';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import DeleteIcon from '@material-ui/icons/Delete';
import UpdateIcon from '@material-ui/icons/Update';
import SwapHorizontalCircleIcon from '@material-ui/icons/SwapHorizontalCircle';
import InsertDriveFileIcon from '@material-ui/icons/InsertDriveFile';
import EmailIcon from '@material-ui/icons/Email';

import {Grid, Button, Container, Avatar, Paper, TextField, Typography, Modal, Divider, Accordion, AccordionDetails, AccordionSummary, ListItemSecondaryAction, InputLabel, MenuItem, FormHelperText, FormControl, Select, Menu} from '@material-ui/core'
import {makeStyles}  from '@material-ui/core/styles';
import NavBar2 from './NavBar2';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Tooltip from '@material-ui/core/Tooltip';

import {
    BrowserRouter as Router,
    Switch,
    useLocation, Link
  } from "react-router-dom";
  
import axios from 'axios';
import Swal from 'sweetalert2';
import Cookies from 'universal-cookie';
import Propiedades from './Propiedades';
 
const cookies = new Cookies();

const url = "http://34.71.225.235:3000/";

function getModalStyle(){
    return{
        top: '20%',
        left: '30%',
        transform: 'tanslate(-45%,-45%)',
    };
}

const useStyles = makeStyles((theme) => ({
    root: {
        position: 'relative',
        backgroundColor: "#b3cde0",
        paddingTop: '20px',
        paddingLeft: '100px',
        paddingRight: '100px',
        paddingBottom: '50px'
    },
    rootGrid: {
        flexGrow: 1
    },
    paperStyle: {
        padding: 20,
        height: "7vh",
        width: 1300,
        margin: "20px auto"
    },
    paperStyle2: {
        padding: 20,
        height: "80vh",
        width: 1300,
        margin: "20px auto"
    },
    btn: {
        margin: '1px 1px',
        backgroundColor: '#005b96',
        color: 'white',
        height: '30px',
        width: '30px'
    },
    btn2: {
        margin: '1px 1px',
        backgroundColor: '#005b96',
        color: 'white',
        height: '50px',
        width: '50px'
    },
    avatar:{
        backgroundColor: '#6497b1',
        width: 60,
        height: 60
    },
    divider: {
        margin: theme.spacing(1, 0),
    },
    createFolder: {
        backgroundColor: '#6497b1',
        backgroundImage: '#3234ab'
    },
    letritas: {
        margin: '2px',
        padding: '6px',
        fontFamily: 'Times New Roman',
        fontSize: '20px'
    },
    paper:{
        position: 'absolute',
        width: 400,
        backgroundColor: "#ffffff",
        border: '2px solid #000000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2,4,3),
    },
    divider: {
        margin: theme.spacing(1, 0),
    },
    link:{
        textDecoration: 'none',
        color: '#000000'
    },
    container:{
        backgroundColor: "#ffffff"
    },
    rootAcordeon: {
        width: '100%',
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        flexBasis: '33.33%',
        flexShrink: 0,
    },
    secondaryHeading: {
        fontSize: theme.typography.pxToRem(15),
        color: theme.palette.text.secondary,
    },
    formControl: {
        minWidth: 400,
        position: 'relative',
        backgroundColor: "white",
        fontSize: 16,
    },
  }));

//const Drive = () => {
export default function Drive () {


    const classes = useStyles();

    const guardarNombreFolder = (nombre) =>{
        cookies.set('carpetaA', nombre, {path: '/'});
    }

    /*const [datos, setDatos] = useState({
        carpetas: [["vacacion","#12ab34"],["Carpeta1","#ba23a3"]]
    })*/
    //carpetas
    const [datos, setDatos] = useState([]);
    //archivos
    const [datos2, setDatos2] = useState([]);
    
    const [modalStyle] = React.useState(getModalStyle);

    const [open, setOpen] = React.useState(false);
    const [open2, setOpen2] = React.useState(false);
    const [open3, setOpen3] = React.useState(false);
    const [correo, setCorreo] = React.useState(false);

    const [carpeta, setCarpeta] = useState({
        name: ''
    })

    const [archivoNombre, setArchivoNombre] = useState({
        name: ''
    })
    
    //Para modales

    const handleOpen =()=>{
        setOpen(true);
    };

    const handleClose=()=>{
        setOpen(false);
    };

    const handleOpen3 =()=>{
        setOpen3(true);
    };

    const handleClose3=()=>{
        setOpen3(false);
    };

    const handleOpen2 =()=>{
        console.log("abrir desde drive")
        setOpen2(true);
    };

    const handleClose2 =()=>{
        setOpen2(false);
    };

    const abrirCorreo = ()=>{
        setCorreo(true);
    };

    const cerrarCorreo = ()=>{
        setCorreo(false);
    };

    //Carpetas
    //eliminar
    const [openEliminar, setOpenEliminar] = React.useState(false);

    const abrirEliminar =()=>{
        setOpenEliminar(true);
    };

    const cerrarEliminar =()=>{
        setOpenEliminar(false);
    };

    const [age, setAge] = React.useState('');

    const seleccionarFolder = (event) =>{
        setAge(event.target.value)
        console.log("folder " + age);
    }

    //actualizar
    const [openActualizar, setOpenActualizar] = React.useState(false);

    const abrirActualizar =()=>{
        console.log("abriendo modal")
        setOpenActualizar(true);
    };

    const cerrarActualizar =() =>{
        setOpenActualizar(false);
    };

    const actualizarCarpeta = async()=>{
        console.log("codigo carpeta " + age + " nuevo nombre " + carpeta.name);
        axios.put(url+"ddb/carpeta/update", {"codigo": age, "propietario": cookies.get("name"), nombre: carpeta.name, cantidad: "0"})
        .then(response=>{
            var resp = { "RESPUESTA": "hola"};
            resp = response.data
            console.log(resp.RESPUESTA)
            if(resp.RESPUESTA == "EXITOSO/"){
                console.log("carpeta actualizada")
                actualizarEnS3()
                setTimeout("location.href='/aydrive'", 1000);
            }else{
                console.log("carpeta no actualizada")
            }
        })
        .catch(error=>{
            console.log(error)
        })       
    }
    
    const actualizarEnS3 = async()=>{
        let nombrecito = '';
        axios.post(url+"ddb/carpeta/ver", {codigo: age})
        .then(response=>{
            nombrecito = response.data[0].nombre
        })
        .catch(error=>{
            console.log(error)
        })

        axios.post(url+"s3/renamefileTODO",{"direccionviejonombre": cookies.get("name") + '/' + nombrecito, "direccionnuevonombre": cookies.get("name") + '/' + carpeta.name + '/'})
        .then(response=>{
            var resp = { "RESPUESTA": "hola"};
            resp = response.data
            console.log(resp.RESPUESTA)
            if(resp.RESPUESTA == "SI SE RENOMBRO/"){
                console.log("carpeta renombrada")
                setTimeout("location.href='/aydrive'", 1000);
            }else{
                console.log("carpeta no renombrada")
            }
        })
        .catch(error=>{
            console.log(error)
        })     
    }

    //Archivos
    //eliminar
    const [openEliminarArchivo, setOpenEliminarArchivo] = React.useState(false);

    const abrirEliminarArchivo =()=>{
        setOpenEliminarArchivo(true);
    };

    const cerrarEliminarArchivo =()=>{
        setOpenEliminarArchivo(false);
    };

    const [file, setFile] = React.useState('');

    const seleccionarArchivo = (event) =>{
        setFile(event.target.value)
        console.log("file " + file);
    }

    const [expanded, setExpanded] = React.useState(false);

    const handleChange = (panel) => (event, isExpanded) => {
        setExpanded(isExpanded ? panel : false);
    };

    useEffect(() => {
        //getCarpetas();
        axios.post(url+"ddb/carpeta/all",{propietario: cookies.get("name")})
        .then(response=>{
            console.log(response.data)
            let limpio = [];
            for(let i = 0; i < response.data.length; i++){
                if(response.data[i].nombre !== "papelera" && response.data[i].nombre !== cookies.get("name")){
                    limpio.push(response.data[i]);
                }
            }
            setDatos(limpio);
            console.log(datos)
            //setDatos({carpetas: resp})
            //console.log("las carpetas")
            //console.log(datos.carpetas)
        })
        .catch(error=>{
            console.log(error);
        })

        //obtener archivos
        axios.post(url+"ddb/archivo/all",{propietario: cookies.get("name")})
        .then(response=>{
            let todo = response.data;
            let limpio = []
            for(let i = 0; i < todo.length; i++){
                if(todo[i].carpeta === cookies.get("carpetaA")){// cookies = nueva .carpeta = nueva
                    limpio.push(todo[i])
                }
            }
            setDatos2(limpio);
            console.log("archivos")
            console.log(datos2)
        })
        .catch(error=>{
            console.log(error);
        })
    },[]);

    const handleInputChange = (event) =>{
        
        setCarpeta({
            ...carpeta,
            [event.target.name]: event.target.value
        })
    }

    const handleInputChangeArchivo = (event) =>{
        
        setArchivoNombre({
            ...archivoNombre,
            [event.target.name]: event.target.value
        })
    }

    //actualizar
    const [openActualizarArchivo, setOpenActualizarArchivo] = React.useState(false);

    const abrirActualizarArchivo =()=>{
        console.log("abriendo modal")
        setOpenActualizarArchivo(true);
    };

    const cerrarActualizarArchivo =() =>{
        setOpenActualizarArchivo(false);
    };

    const actualizarArchivo = ()=>{

        console.log("codigo archivo " + age + " nuevo nombre " + carpeta.name);
        //buscamos el archivo
        var datosArchivo;
        axios.post(url+"ddb/archivo/ver",{codigo: age})
        .then(response=>{
            datosArchivo = response.data[0]
            console.log(datosArchivo)
            
            var nombreV = cookies.get("name") + "/" + datosArchivo.nombre+"."+datosArchivo.extension;
            var nombreN = cookies.get("name") + "/" + carpeta.name+"."+datosArchivo.extension;

            console.log(nombreV);
            console.log(nombreN);
            //update archivo S3
            axios.post(url+"s3/renamefileTODO",{direccionviejonombre: nombreV, direccionnuevonombre: nombreN})
            .then(response=>{
                var resp = { "RESPUESTA": "hola"};
                resp = response.data
                console.log(resp)
                if(resp.RESPUESTA == "SI SE RENOMBRO/"){
                    console.log("Si se cambio el nombre en s3")
                }else{
                    console.log("archivo no se renombro")
                }
            })
            .catch(error=>{
                console.log(error)
            })
        //update en base de datos
        axios.put(url+"ddb/archivo/update",{codigo: age, propietario: cookies.get("name"), nombre: carpeta.name, carpeta: cookies.get("name"), link: datosArchivo.link})
            .then(response=>{
                var resp = { "RESPUESTA": "hola"};
                resp = response.data
                console.log(resp)
                if(resp.RESPUESTA == "EXITOSO/"){
                    console.log("SE RENOMBRO EL ARCHVIO")
                    setTimeout("location.href='./aydrive'", 1000);
                }else{
                    console.log("********** NO SE RENOMBRO")
                }
            })
            .catch(error=>{
                console.log(error)
            })         
        })
        .catch(error=>{
            console.log(error)
        })
    }

    /*****************************Carpeta */

    const crearCarpeta = (event) =>{
        console.log(carpeta.name)
        console.log(cookies.get("name"))
        console.log(cookies.get("carpetaA"))
        newFolder();
        save();
    }

    const newFolder = async()=>{
        axios.post(url+"s3/crearcarpetanueva", {"name": carpeta.name, "name2": cookies.get("carpetaA") + "/"})
        .then(response=>{
            var resp = { "RESPUESTA": "hola"};
            resp = response.data
            console.log(resp.RESPUESTA)
            if(resp.RESPUESTA == "SI SE CREO/"){
                Swal.fire({
                    icon: 'success',
                    title: 'Created',
                    showConfirmButton: false,
                    timer: 1000
                })
                console.log("carpeta creada")
                setTimeout("location.href='/aydrive'", 1000);
            }else{
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    showConfirmButton: false,
                    timer: 2000
                })
                console.log("carpeta no creada")
            }
        })
        .catch(error=>{
            console.log(error)
        })
        
    }

    const save = async()=>{
        var f = new Date();
        var fechaactual = f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear();
        axios.post(url+"ddb/carpeta/save",{nombre: carpeta.name, cantidad:"0", fecha: fechaactual, propietario: cookies.get("name")})
        .then(response=>{
            var resp = { "RESPUESTA": "hola"};
            resp = response.data
            console.log(resp.RESPUESTA)
            if(resp.RESPUESTA == "EXITOSO/"){
                console.log("carpeta creada en bd")
            }else{
                console.log("carpeta no creada en bd")
            }
        })
        .catch(error=>{
            console.log(error);
        })
    }

    const getCarpetas = async()=>{
        axios.post(url,{propietario: cookies.get("name")})
        .then(response=>{
            var resp = { "RESPUESTA": "hola"};
            resp = response.data
            console.log(resp)
            //setDatos({carpetas: resp})
            //console.log("las carpetas")
            //console.log(datos.carpetas)
        })
        .catch(error=>{
            console.log(error);
        })
    }

    const eliminarCarpeta=()=>{
        console.log("eliminar carpeta con codigo: " + age)
        axios.post(url+"ddb/carpeta/delete",{codigo: age, propietario: cookies.get("name")})
        .then(response=>{
            var resp = { "RESPUESTA": "hola"};
            resp = response.data
            console.log(resp)
            if(resp.RESPUESTA == "EXITOSO/"){
                console.log("carpeta eliminada")
                setTimeout("location.href='/aydrive'", 1000);
            }else{
                console.log("carpeta no elimana")
                setTimeout("location.href='/aydrive'", 1000);
            }
        })
        .catch(error=>{
            console.log(error)
        })
    }

    /*****************************Archivos */
    let enBase64 = '';
    let ext = '';
    const convertirBase64=(archivos)=>{
        Array.from(archivos).forEach(archivo=>{
            var reader = new FileReader();
            reader.readAsDataURL(archivo);
            reader.onload=function(){
                var aux=[];
                var base64 = reader.result;
                //imagen = base64;
                console.log("a base 64");
                console.log(base64);
                aux = base64.split(',');
                enBase64 = aux[1];
                console.log(enBase64);
                var aux2, aux3 = [];
                aux2 =aux[0].split('/');
                aux3 = aux2[1].split(';');
                ext = aux3[0]
                extension();
                console.log('la extension es: ' + ext);
                console.log("archivo " + archivoNombre.name);
            }
        })
    }

    /**
     * docx
     * la extension es: vnd.openxmlformats-officedocument.wordprocessingml.document
     * pptx
     * la extension es: vnd.openxmlformats-officedocument.presentationml.presentation
     * xlsx
     * la extension es: vnd.openxmlformats-officedocument.spreadsheetml.sheet
     * doc
     * msword
     */
    const extension = ()=>{
        if(ext === "vnd.openxmlformats-officedocument.wordprocessingml.document"){
            ext = "docx"
        }else if(ext === "vnd.openxmlformats-officedocument.presentationml.presentation"){
            ext = "pptx"
        }else if(ext == "vnd.openxmlformats-officedocument.spreadsheetml.sheet"){
            ext = "xlsx"
        }else if(ext == "msword"){
            ext = "doc"
        }
    }

    const crearArchivo = async()=>{
        console.log("vamos a crear archivo")
        if(enBase64 != '' && ext != '' && archivoNombre.name != ''){
            console.log("se deberia crear")
            console.log(cookies.get("carpetaA"))
            //carpetaA=root == name 
            //carpetaA=nueva != name
            //root/ + carpetaA + /
            axios.post(url+"s3/crearnuevoarchivosinrepetir",{name: archivoNombre.name, ext: ext, name2: cookies.get("carpetaA") + '/', base64: enBase64})
            .then(response=>{
                var hola = '{servicio: "s3", accion: "crearnuevoarchivosinrepetir", campos: {name: '+ archivoNombre.name + ', ext: ' + ext + ', name2: ' + cookies.get("carpetaA")  + '/, base64: ' + enBase64 +'}}'
                console.log(hola)
                var resp = { "RESPUESTA": "hola"};
                resp = response.data
                console.log(resp.RESPUESTA)
                if(resp.RESPUESTA == "SI SE CREO/"){
                    Swal.fire({
                        icon: 'success',
                        title: 'Created',
                        showConfirmButton: false,
                        timer: 1000
                    })
                    console.log("archivo creado")
                    obtenerURL();
                    setTimeout("location.href='/aydrive'", 1000);
                }else{
                    Swal.fire({
                        icon: 'error',
                        title: 'Error',
                        showConfirmButton: false,
                        timer: 2000
                    })
                    console.log("archivo no creado")
                }
            })
            .catch(error=>{
                console.log(error);
            })
        }
    }

    let URL = ''
    const obtenerURL = async()=>{
        axios.post(url+"s3/obtenerURL",{name: archivoNombre.name + '.' + ext, dire: cookies.get("name") + '/'})
        .then(response=>{
            var resp = { "Direccion": "hola"};
            resp = response.data
            console.log(resp.Direccion)
            URL = resp.Direccion
            saveFile();
        })
        .catch(error=>{
            console.log(error);
        })
    }

    const saveFile = async()=>{
        var f = new Date();
        var fechaactual = f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear();
        axios.post(url+"ddb/archivo/save",{nombre: archivoNombre.name, extension: ext, propietario: cookies.get("name"), carpeta: cookies.get("carpetaA"), fecha: fechaactual, link: URL})
        .then(response=>{
            var resp = { "RESPUESTA": "hola"};
            resp = response.data
            console.log(resp.RESPUESTA)
            if(resp.RESPUESTA == "EXITOSO/"){
                console.log("archivo creado en bd")
            }else{
                console.log("archivo no creado en bd")
            }
        })
        .catch(error=>{
            console.log(error);
        })
    }

    const eliminarArchivo=()=>{
        //ahora se mueve a la papelera
        console.log("mover con codigo: " + file + " a la papelera")
        var datosArchivo;

        axios.post(url+"ddb/archivo/ver",{codigo: file})
        .then(response=>{
            datosArchivo = response.data[0]
            //setArchivoDatos(response.data)
            
            var direccionV = cookies.get("name") + "/" + datosArchivo.nombre+"."+datosArchivo.extension;
            var direccionN = cookies.get("name") + "/papelera/" + datosArchivo.nombre+"."+datosArchivo.extension;
            console.log(direccionV)
            console.log(direccionN)
            //movemos archivo S3
            axios.post(url+"s3/moverlosarchivos",{direccionvieja: direccionV, direccionnueva: direccionN})
            .then(response=>{
                var resp = { "RESPUESTA": "hola"};
                resp = response.data
                console.log(resp)
                if(resp.RESPUESTA == "SI SE MOVIO DE LUGAR/"){
                    console.log("Si se movio el archivo en s3 mover en base de datos")
                    //setTimeout(`location.href='./${name}'`, 1000);
                }else{
                    console.log("archivo no se movio")
                }
            })
            .catch(error=>{
                console.log(error)
            })
            //movemos en base de datos
            //var URL = ''
            //concatenar la carpetaA/nombre.extension
              /*  axios.post(url,{servicio: "s3", accion: "obtenerURL", campos: {name: datosArchivo.nombre + '.' + datosArchivo.extension, dire: cookies.get("name") + '/papelera/'}})
                .then(response=>{
                    var resp = { "Direccion": "hola"};
                    resp = response.data
                    console.log(resp.Direccion)
                    URL = resp.Direccion
                    console.log("esto si" + URL)
                })
                .catch(error=>{
                    console.log("aqui")
                    console.log(error);
                })
            console.log("URL " + URL)*/
            axios.put(url+"ddb/archivo/update",{codigo: file, propietario: cookies.get("name"), nombre: cookies.get("name") + '/' + datosArchivo.nombre, carpeta: 'papelera', link: datosArchivo.link})
            .then(response=>{
                var resp = { "RESPUESTA": "hola"};
                resp = response.data
                console.log(resp)
                if(resp.RESPUESTA == "EXITOSO/"){
                    console.log("SE MOVIO EL ARCHIVO A PAPELERA")
                    setTimeout("location.href='/aydrive'", 1000);
                }else{
                    console.log("archivo no se movio")
                }
            })
            .catch(error=>{
                console.log(error)
            })            
        })
        .catch(error=>{
            console.log(error)
        })


        /*
        console.log("eliminar archivo con codigo: " + file)
        axios.post(url,{servicio: "archivo", accion: "delete", campos:{codigo: file, propietario: cookies.get("name")}})
        .then(response=>{
            var resp = { "RESPUESTA": "hola"};
            resp = response.data
            console.log(resp)
            if(resp.RESPUESTA == "EXITOSO/"){
                console.log("archivo eliminado")
                setTimeout("location.href='/aydrive'", 1000);
            }else{
                console.log("archivo no eliminado")
                setTimeout("location.href='/aydrive'", 1000);
            }
        })
        .catch(error=>{
            console.log(error)
        })
        */
    }

    const enviarArchivo=()=>{
        console.log('hola')
        var datosArchivo;
        axios.post(url+"ddb/archivo/ver",{codigo: file})
        .then(response=>{
            datosArchivo = response.data[0]
            var link = datosArchivo.link
            console.log(link);
            axios.post(url+"sendcorreo", {correo: cookies.get("email"), asunto: datosArchivo.nombre, cuerpo: link})
            .then(response=>{
                var resp = { "RESPUESTA": "hola"};
                resp = response.data
                console.log(resp)
                if(resp.RESPUESTA == "EXITOSO/"){
                    console.log("SE ENVIO EL CORREO")
                }else{
                    console.log("NO SE ENVIO EL CORREO")
                }
            })
            .catch(error=>{
                console.log(error)
            })
        })
        .catch(error=>{
            console.log(error)
        })
    }
    //mover archivo
    const [openMoverArchivo, setOpenMoverArchivo] = useState(false);

    const abrirMoverArchivo =()=>{
        setOpenMoverArchivo(true);
    };

    const cerrarMoverArchivo =()=>{
        setOpenMoverArchivo(false);
    };

    const moverArchivo=()=>{
        console.log("mover con codigo: " + ArchivoMover + " a la carpeta: " + Carpeta2 )
        var datosArchivo;

        axios.post(url+"ddb/archivo/ver",{codigo: ArchivoMover})
        .then(response=>{
            datosArchivo = response.data[0]
            //setArchivoDatos(response.data)
            
            var direccionV = cookies.get("name") + "/" + datosArchivo.nombre+"."+datosArchivo.extension;
            var direccionN = cookies.get("name") + "/" + Carpeta2 + "/" + datosArchivo.nombre+"."+datosArchivo.extension;
            console.log(direccionV)
            console.log(direccionN)
            //movemos archivo S3
            axios.post(url+"s3/moverlosarchivos",{direccionvieja: direccionV, direccionnueva: direccionN})
            .then(response=>{
                var resp = { "RESPUESTA": "hola"};
                resp = response.data
                console.log(resp)
                if(resp.RESPUESTA == "SI SE MOVIO DE LUGAR/"){
                    console.log("Si se movio el archivo en s3 mover en base de datos")
                    //setTimeout(`location.href='./${name}'`, 1000);
                }else{
                    console.log("archivo no se movio")
                }
            })
            .catch(error=>{
                console.log(error)
            })
            //movemos en base de datos
            axios.put(url+"ddb/archivo/update",{codigo: ArchivoMover, propietario: cookies.get("name"), nombre: datosArchivo.nombre, carpeta: Carpeta2, link: datosArchivo.link})
            .then(response=>{
                var resp = { "RESPUESTA": "hola"};
                resp = response.data
                console.log(resp)
                if(resp.RESPUESTA == "EXITOSO/"){
                    console.log("SE MOVIO EL ARCHIVO")
                    setTimeout("location.href='/aydrive'", 1000);
                }else{
                    console.log("archivo no se movio")
                }
            })
            .catch(error=>{
                console.log(error)
            })            
        })
        .catch(error=>{
            console.log(error)
        })

        
    }

    const [ArchivoMover, setArchivoMover] = useState('');

    const seleccionarArchivoMover = (event) =>{
        setArchivoMover(event.target.value)
    }

    const [Carpeta2, setCarpeta2] = useState('');

    const seleccionarCarpeta2 = (event) =>{
        setCarpeta2(event.target.value)
    }

    return(
        <>
            <NavBar2/>
            <div className={classes.root}>
                <Paper elevation={10} className={classes.paperStyle}>
                    <Grid aling="left">
                        <Tooltip title="New Folder">
                            <Button  onClick={handleOpen}>
                                <CreateNewFolderIcon style={{ color: '#6497b1', fontSize: 40 }}/>
                            </Button>
                        </Tooltip>
                        <Modal
                            open={open}
                            onClose={handleClose}
                        >
                            <div style={modalStyle} className={classes.paper}>
                                <p>New Folder</p>
                                <TextField onChange={handleInputChange} name="name" placeholder="Please enter name." label="Name" fullWidth required></TextField>
                                <Divider className={classes.divider}/>
                                <Button onClick={crearCarpeta} className={classes.btn} type="submit"  variant="contained" fullWidth>Create</Button>
                        
                            </div>
                            </Modal>
                        <Tooltip title="Delete Folder">
                            <Button onClick={abrirEliminar}>
                                <DeleteIcon style={{ color: '#6497b1', fontSize: 40 }} />
                            </Button>
                        </Tooltip>
                        <Modal
                            open={openEliminar}
                            onClose={cerrarEliminar}
                        >
                            <div style={modalStyle} className={classes.paper}>
                                <p>Delete Folder</p>
                                <FormControl variant="filled" className={classes.formControl}>
                                    <InputLabel id="demo-simple-select-filled-label">Folder</InputLabel>
                                    <Select
                                    labelId="demo-simple-select-filled-label"
                                    id="demo-simple-select-filled"
                                    value={age}
                                    onChange={seleccionarFolder}
                                    >
                                    <MenuItem value="">
                                        <em>None</em>
                                    </MenuItem>
                                    {datos.map((f,i)=>{
                                        return(
                                            <MenuItem value={f.codigo}>{f.nombre}</MenuItem>
                                        )
                                    })}
                                    </Select>
                                </FormControl>
                                <Divider className={classes.divider}/>
                                <Button onClick={eliminarCarpeta} className={classes.btn} type="submit"  variant="contained" fullWidth>Delete</Button>
                        
                            </div>
                            </Modal>
                        <Tooltip title="Update Folder">
                            <Button onClick={abrirActualizar}>
                                <UpdateIcon style={{ color: '#6497b1', fontSize: 40 }} />
                            </Button>
                        </Tooltip>
                        <Modal
                            open={openActualizar}
                            onClose={cerrarActualizar}
                        >
                            <div style={modalStyle} className={classes.paper}>
                                <p>Update Folder</p>
                                <TextField onChange={handleInputChange} name="name" placeholder="Please enter name." label="Name" fullWidth required></TextField>
                                <Divider/>
                                <FormControl variant="filled" className={classes.formControl}>
                                    <InputLabel id="demo-simple-select-filled-label">Folder</InputLabel>
                                    <Select
                                    labelId="demo-simple-select-filled-label"
                                    id="demo-simple-select-filled"
                                    value={age}
                                    onChange={seleccionarFolder}
                                    >
                                    <MenuItem value="">
                                        <em>None</em>
                                    </MenuItem>
                                    {datos.map((f,i)=>{
                                        return(
                                            <MenuItem value={f.codigo}>{f.nombre}</MenuItem>
                                        )
                                    })}
                                    </Select>
                                </FormControl>
                                <Divider className={classes.divider}/>
                                <Button onClick={actualizarCarpeta} className={classes.btn} type="submit"  variant="contained" fullWidth>Update</Button>
                        
                            </div>
                            </Modal>
                        <Button disabled>
                            <MoreHorizIcon style={{ color: '#000', fontSize: 40 }}/>
                        </Button>
                        <Tooltip title="New File">
                            <Button onClick={handleOpen3}>
                            <NoteAddIcon style={{ color: '#6497b1', fontSize: 40 }} />
                            </Button>
                        </Tooltip>
                        <Modal
                            open={open3}
                            onClose={handleClose3}
                        >
                            <div style={modalStyle} className={classes.paper}>
                                <p>New File</p>
                                <TextField onChange={handleInputChangeArchivo} name="name" placeholder="Please enter name." label="Name" fullWidth required></TextField>
                                <Divider className={classes.divider}/>
                                <input type="file" id="Photo" accept="image/*, application/pdf, .xlsx,.xls, .doc, .docx, .ppt, .pptx,.txt, .mp4, text/plain" multiple onChange={(e)=>convertirBase64(e.target.files)}></input> 
                                <Button onClick={crearArchivo} className={classes.btn} type="submit"  variant="contained" fullWidth>Create</Button>
                        
                            </div>
                            </Modal>
                            <Tooltip title="Delete File">
                                <Button onClick={abrirEliminarArchivo}>
                                    <DeleteIcon style={{ color: '#6497b1', fontSize: 40 }} />
                                </Button>
                            </Tooltip>
                            <Modal
                            open={openEliminarArchivo}
                            onClose={cerrarEliminarArchivo}
                            >
                            <div style={modalStyle} className={classes.paper}>
                                <p>Delete File</p>
                                <FormControl variant="filled" className={classes.formControl}>
                                    <InputLabel id="demo-simple-select-filled-label">File</InputLabel>
                                    <Select
                                    labelId="demo-simple-select-filled-label"
                                    id="demo-simple-select-filled"
                                    value={file}
                                    onChange={seleccionarArchivo}
                                    >
                                    <MenuItem value="">
                                        <em>None</em>
                                    </MenuItem>
                                    {datos2.map((a,i)=>{
                                        return(
                                            <MenuItem value={a.codigo}>{a.nombre}</MenuItem>
                                        )
                                    })}
                                    </Select>
                                </FormControl>
                                <Divider className={classes.divider}/>
                                <Button onClick={eliminarArchivo} className={classes.btn} type="submit"  variant="contained" fullWidth>Delete</Button>
                        
                            </div>
                            </Modal>
                            <Tooltip title="Update File">
                                <Button onClick={abrirActualizarArchivo}>
                                    <UpdateIcon style={{ color: '#6497b1', fontSize: 40 }} />
                                </Button>
                            </Tooltip>
                                <Modal
                                open={openActualizarArchivo}
                                onClose={cerrarActualizarArchivo}
                            >
                                <div style={modalStyle} className={classes.paper}>
                                    <p>Update File</p>
                                    <TextField onChange={handleInputChange} name="name" placeholder="Please enter name." label="Name" fullWidth required></TextField>
                                    <Divider/>
                                    <FormControl variant="filled" className={classes.formControl}>
                                        <InputLabel id="demo-simple-select-filled-label">File</InputLabel>
                                        <Select
                                        labelId="demo-simple-select-filled-label"
                                        id="demo-simple-select-filled"
                                        value={age}
                                        onChange={seleccionarFolder}
                                        >
                                        <MenuItem value="">
                                            <em>None</em>
                                        </MenuItem>
                                        {datos2.map((f,i)=>{
                                            return(
                                                <MenuItem value={f.codigo}>{f.nombre}</MenuItem>
                                            )
                                        })}
                                        </Select>
                                    </FormControl>
                                    <Divider className={classes.divider}/>
                                    <Button onClick={actualizarArchivo} className={classes.btn} type="submit"  variant="contained" fullWidth>Update</Button>
                            
                                </div>
                                </Modal>
                            <Tooltip title="Move File">
                                <Button onClick={abrirMoverArchivo}>
                                    <SwapHorizontalCircleIcon style={{ color: '#6497b1', fontSize: 40 }} />
                                </Button>
                            </Tooltip>
                            <Modal
                            open={openMoverArchivo}
                            onClose={cerrarMoverArchivo}
                            >
                                <div style={modalStyle} className={classes.paper}>
                                    <Typography variant="h5" color="initial" align="center">Move File</Typography>
                                    <Divider className={classes.divider2}/>
                                    <FormControl className={classes.formControl} variant="filled">
                                        <Select
                                        className={classes.selectEmpty}
                                        //labelId="demo-simple-select-filled-label"
                                        id="demo-simple-select-filled"
                                        value={ArchivoMover}
                                        onChange={seleccionarArchivoMover}
                                        fullWidth
                                        >
                                        <MenuItem value="">
                                            <em>None</em>
                                        </MenuItem>
                                        {datos2.map((a,i)=>{
                                            return(
                                                <MenuItem value={a.codigo}>{a.nombre}</MenuItem>
                                            )
                                        })}
                                        </Select>
                                        <FormHelperText>Select File</FormHelperText>
                                        
                                        <Select
                                        className={classes.selectEmpty}
                                        //labelId="demo-simple-select-filled-label"
                                        id="demo-simple-select-filled"
                                        value={Carpeta2}
                                        onChange={seleccionarCarpeta2}
                                        fullWidth
                                        >
                                        <MenuItem value="">
                                            <em>None</em>
                                        </MenuItem>
                                        {datos.map((a,i)=>{
                                            return(
                                                <MenuItem value={a.nombre}>{a.nombre}</MenuItem>
                                            )
                                        })}
                                        </Select>
                                        <FormHelperText>Select Folder</FormHelperText>
                                    </FormControl>
                                    <Divider className={classes.divider}/>
                                    <Button onClick={moverArchivo} className={classes.btn2} type="submit"  variant="contained" >Move File</Button>
                                </div>
                            </Modal>
                            <Tooltip title="Email">
                            <Button  onClick={abrirCorreo}>
                                <EmailIcon style={{ color: '#6497b1', fontSize: 40 }}/>
                            </Button>
                            </Tooltip>
                            <Modal
                            open={correo}
                            onClose={cerrarCorreo}
                            >
                            <div style={modalStyle} className={classes.paper}>
                                <p>Send File</p>
                                <FormControl variant="filled" className={classes.formControl}>
                                    <InputLabel id="demo-simple-select-filled-label">File</InputLabel>
                                    <Select
                                    labelId="demo-simple-select-filled-label"
                                    id="demo-simple-select-filled"
                                    value={file}
                                    onChange={seleccionarArchivo}
                                    >
                                    <MenuItem value="">
                                        <em>None</em>
                                    </MenuItem>
                                    {datos2.map((a,i)=>{
                                        return(
                                            <MenuItem value={a.codigo}>{a.nombre}</MenuItem>
                                        )
                                    })}
                                    </Select>
                                </FormControl>
                                <Divider className={classes.divider}/>
                                <Button onClick={enviarArchivo} className={classes.btn} type="submit"  variant="contained" fullWidth>Send</Button>
                        
                            </div>
                            </Modal>

                    </Grid>
                </Paper>
                
                <Container maxWidth="lg" className={classes.container}>
                    {datos.map((c,i)=>{
                        return(
                            <div key={i} className={classes.rootAcordeon}>
                                <Accordion expanded={expanded === i} onChange={handleChange(i)}>
                                    <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel1bh-content"
                                    id="panel1bh-header"
                                    >
                                    <Grid item xs={11}>
                                        <Link className={classes.link}  to={`/folder/${c.nombre}`} >
                                            <Grid container>
                                                <Grid item xs={1}>
                                                    <FolderIcon style={{color: '#6497b1', fontSize: 40}}/>
                                                </Grid>
                                                <Grid item xs={11}>
                                                    <div className={classes.letritas}>{c.nombre}</div>
                                                </Grid>
                                            </Grid>
                                        </Link>
                                    </Grid>
                                    </AccordionSummary>
                                    <AccordionDetails>
                                    <Grid aling="center">
                                    
                                                <p>----------------- Properties -----------------</p>
                                                <Divider/>
                                                <p>Name: {datos[i].nombre}</p>
                                                <p>Date: {datos[i].fecha}</p>
                                                <p>Code: {datos[i].codigo}</p>
                                                <p>Owner: {datos[i].propietario}</p>
                                                <Divider/>
                                    </Grid>
                                    </AccordionDetails>
                                </Accordion>   
                            </div>
                        )
                    })}
                </Container>
                <Container maxWidth="lg" className={classes.container}>
                    {datos2.map((a,i)=>{
                        return(
                            <div key={i+100} className={classes.rootAcordeon}>
                                <Accordion expanded={expanded === i+100} onChange={handleChange(i+100)}>
                                    <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel1bh-content"
                                    id="panel1bh-header"
                                    >
                                    <Grid item xs={11}>
                                        <a className={classes.link} href={a.link}>
                                            <Grid container>
                                                <Grid item xs={1}>
                                                    <InsertDriveFileIcon style={{color: '#6497b1', fontSize: 40}}/>
                                                </Grid>
                                                <Grid item xs={11}>
                                                    <div className={classes.letritas}>{a.nombre}.{a.extension}</div>
                                                </Grid>
                                            </Grid>
                                        </a>
                                    </Grid>
                                    </AccordionSummary>
                                    <AccordionDetails>
                                    <Grid aling="center">
                                    
                                                <p>----------------- Properties -----------------</p>
                                                <Divider/>
                                                <p>Name: {datos2[i].nombre}</p>
                                                <p>Extension: {datos2[i].extension}</p>
                                                <p>Date: {datos2[i].fecha}</p>
                                                <p>Code: {datos2[i].codigo}</p>
                                                <p>Owner: {datos2[i].propietario}</p>
                                                <p>Link: {datos2[i].link}</p>
                                                <p>Folder: {datos2[i].carpeta}</p>
                                                <Divider/>
                                    </Grid>
                                    </AccordionDetails>
                                </Accordion>   
                            </div>
                        )
                    })}
                </Container>
            </div>
        </>
    )
}

//export default Drive 

//<Propiedades
                                        //nombre={datos[i].nombre}
                                        //fecha={c.fecha}
                                        //open={open2}></Propiedades>