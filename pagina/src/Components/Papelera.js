import React, {useState, useEffect} from 'react'

import {Grid, Button, Container, Paper, TextField, Typography, Modal, Divider, Accordion, AccordionDetails, AccordionSummary, ListItemSecondaryAction, InputLabel, MenuItem, FormHelperText, FormControl, Select, Menu, Tooltip} from '@material-ui/core'
import {makeStyles}  from '@material-ui/core/styles';
import NavBar2 from './NavBar2';
import UpdateIcon from '@material-ui/icons/Update';
import DeleteIcon from '@material-ui/icons/Delete';
import Cookies from 'universal-cookie';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import InsertDriveFileIcon from '@material-ui/icons/InsertDriveFile';

import axios from 'axios';
import Swal from 'sweetalert2';
const cookies = new Cookies();

const url = "http://34.71.225.235:3000/";

const useStyles = makeStyles((theme) => ({
    root: {
        position: 'relative',
        backgroundColor: "#b3cde0",
        paddingTop: '20px',
        paddingLeft: '100px',
        paddingRight: '100px',
        paddingBottom: '50px'
    },
    rootGrid: {
        flexGrow: 1
    },
    paperStyle: {
        padding: 20,
        height: "7vh",
        width: 1300,
        margin: "20px auto"
    },
    paperStyle2: {
        padding: 20,
        height: "80vh",
        width: 1300,
        margin: "20px auto"
    },
    btn: {
        margin: '1px 1px',
        backgroundColor: '#005b96',
        color: 'white',
        height: '30px',
        width: '30px'
    },
    btn2: {
        margin: '1px 1px',
        backgroundColor: '#005b96',
        color: 'white',
        height: '50px',
        width: '50px'
    },
    avatar:{
        backgroundColor: '#6497b1',
        width: 60,
        height: 60
    },
    divider: {
        margin: theme.spacing(1, 0),
    },
    createFolder: {
        backgroundColor: '#6497b1',
        backgroundImage: '#3234ab'
    },
    letritas: {
        margin: '2px',
        padding: '6px',
        fontFamily: 'Times New Roman',
        fontSize: '20px'
    },
    paper:{
        position: 'absolute',
        width: 400,
        backgroundColor: "#ffffff",
        border: '2px solid #000000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2,4,3),
    },
    divider: {
        margin: theme.spacing(1, 0),
    },
    link:{
        textDecoration: 'none',
        color: '#000000'
    },
    container:{
        backgroundColor: "#ffffff"
    },
    rootAcordeon: {
        width: '100%',
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        flexBasis: '33.33%',
        flexShrink: 0,
    },
    secondaryHeading: {
        fontSize: theme.typography.pxToRem(15),
        color: theme.palette.text.secondary,
    },
    formControl: {
        minWidth: 400,
        position: 'relative',
        backgroundColor: "white",
        fontSize: 16,
    },
  }));

  function getModalStyle(){
    return{
        top: '20%',
        left: '30%',
        transform: 'tanslate(-45%,-45%)',
    };
}

export default function Papelera(){

    const classes = useStyles();

    //acordeon
    const [expanded, setExpanded] = React.useState(false);

    const handleChange = (panel) => (event, isExpanded) => {
        setExpanded(isExpanded ? panel : false);
    };

    const [modalStyle] = React.useState(getModalStyle);

    //archivos
    const [datos, setDatos] = useState([]);

    //restore
    const [openRestore, setOpenRestore] = React.useState(false);

    const abrirRestore=()=>{
        setOpenRestore(true);
    }

    const closeRestore=()=>{
        setOpenRestore(false);
    }

    const restaurarArchivo=()=>{
        console.log("mover con codigo: " + idFile + " a la papelera")
        var datosArchivo;

        axios.post(url+"ddb/archivo/ver",{codigo: idFile})
        .then(response=>{
            datosArchivo = response.data[0]
            //setArchivoDatos(response.data)
            var nombre = datosArchivo.nombre
            var aux = [];
            aux = nombre.split('/');
            var raiz = ""
            if(aux[0] !== cookies.get("name")){
                raiz = cookies.get("name") + "/";
            }
            var direccionV = cookies.get("name") + "/papelera/" + aux[1] +"."+datosArchivo.extension;
            var direccionN = raiz + aux[0] + "/" + aux[1] +"."+datosArchivo.extension;
            console.log(direccionV)
            console.log(direccionN)
            //movemos archivo S3
            axios.post(url+"s3/moverlosarchivos",{direccionvieja: direccionV, direccionnueva: direccionN})
            .then(response=>{
                var resp = { "RESPUESTA": "hola"};
                resp = response.data
                console.log(resp)
                if(resp.RESPUESTA == "SI SE MOVIO DE LUGAR/"){
                    console.log("Si se movio el archivo en s3 mover en base de datos")
                    //setTimeout(`location.href='./${name}'`, 1000);
                }else{
                    console.log("archivo no se movio")
                }
            })
            .catch(error=>{
                console.log(error)
            })
            //movemos en base de datos
            axios.put(url+"ddb/archivo/update",{codigo: idFile, propietario: cookies.get("name"), nombre: aux[1], carpeta: aux[0], link: datosArchivo.link})
            .then(response=>{
                var resp = { "RESPUESTA": "hola"};
                resp = response.data
                console.log(resp)
                if(resp.RESPUESTA == "EXITOSO/"){
                    console.log("SE MOVIO EL ARCHIVO A PAPELERA")
                    setTimeout("location.href='/papelera'", 1000);
                }else{
                    console.log("archivo no se movio")
                }
            })
            .catch(error=>{
                console.log(error)
            })            
        })
        .catch(error=>{
            console.log(error)
        })

    }

    //delete
    const [openDelete, setOpenDelete] = React.useState(false);

    const abrirDelete=()=>{
        setOpenDelete(true);
    }

    const closeDelete=()=>{
        setOpenDelete(false);
    }

    const eliminarArchivo=()=>{
        console.log("eliminar archivo con codigo: " + idFile)
        axios.post(url+"ddb/archivo/delete",{codigo: idFile, propietario: cookies.get("name")})
        .then(response=>{
            var resp = { "RESPUESTA": "hola"};
            resp = response.data
            console.log(resp)
            if(resp.RESPUESTA == "EXITOSO/"){
                console.log("archivo eliminado")
                setTimeout("location.href='/papelera'", 1000);
            }else{
                console.log("archivo no eliminado")
                setTimeout("location.href='/papelera'", 1000);
            }
        })
        .catch(error=>{
            console.log(error)
        })
    }

    const [idFile, setIdFile] = React.useState('');

    const seleccionarArchivo = (event)=>{
        setIdFile(event.target.value)
        console.log("folder "+idFile);
    }

    useEffect(()=>{
        //recuperar archivos guardar en datos
        axios.post(url+"ddb/archivo/all",{propietario: cookies.get("name")})
        .then(response=>{
            let todo = response.data;
            let limpio = []
            for(let i=0; i<todo.length;i++){
                if(todo[i].carpeta === 'papelera'){
                    limpio.push(todo[i]);
                }
            }
            setDatos(limpio);
            console.log(datos);
        })
        .catch(error=>{
            console.log(error);
        })
    },[]);

    return(
        <>
            <NavBar2/>
            <div className={classes.root}>
                <Paper elevation={10} className={classes.paperStyle}>
                    <Grid aling="left">
                        <Tooltip title="Restore File">
                            <Button onClick={abrirRestore}>
                                <UpdateIcon style={{ color: '#6497b1', fontSize: 40 }} />
                            </Button>
                        </Tooltip>   
                        <Modal
                            open={openRestore}
                            onClose={closeRestore}>
                            <div style={modalStyle} className={classes.paper}>
                                <p>Restore File</p>
                                <FormControl varian="filled" className={classes.formControl}>
                                    <InputLabel id="-label">File</InputLabel>
                                    <Select
                                    lavelId="-label"
                                    id="-"
                                    value={idFile}
                                    onChange={seleccionarArchivo}
                                    >
                                    <MenuItem value="">
                                    <em>None</em>
                                    </MenuItem>
                                    {datos.map((f,i)=>{
                                        return(
                                            <MenuItem value={f.codigo}>{f.nombre}</MenuItem>
                                        )
                                    })}
                                    </Select>
                                </FormControl>
                                <Divider className={classes.divider}/>
                                <Button onClick={restaurarArchivo} className={classes.btn} type="submit" variant="contained" fullWidth>Restore</Button>
                            </div>
                        </Modal> 
                        <Tooltip title="Delete File">
                        <Button onClick={abrirDelete}>
                            <DeleteIcon style={{ color: '#6497b1', fontSize: 40 }} />
                        </Button>
                        </Tooltip>   
                        <Modal
                            open={openDelete}
                            onClose={closeDelete}>
                            <div style={modalStyle} className={classes.paper}>
                                <p>Restore File</p>
                                <FormControl varian="filled" className={classes.formControl}>
                                    <InputLabel id="-label">File</InputLabel>
                                    <Select
                                    lavelId="-label"
                                    id="-"
                                    value={idFile}
                                    onChange={seleccionarArchivo}
                                    >
                                    <MenuItem value="">
                                    <em>None</em>
                                    </MenuItem>
                                    {datos.map((f,i)=>{
                                        return(
                                            <MenuItem value={f.codigo}>{f.nombre}</MenuItem>
                                        )
                                    })}
                                    </Select>
                                </FormControl>
                                <Divider className={classes.divider}/>
                                <Button onClick={eliminarArchivo} className={classes.btn} type="submit" variant="contained" fullWidth>Delete</Button>
                            </div>
                        </Modal> 
                    </Grid>
                </Paper>

                <Container maxWidth="lg" className={classes.container}>
                    {datos.map((a,i)=>{
                        return(
                            <div key={i+2} className={classes.rootAcordeon}>
                                <Accordion expanded={expanded === i+2} onChange={handleChange(i+2)}>
                                    <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel1bh-content"
                                    id="panel1bh-header"
                                    >
                                    <Grid item xs={11}>
                                        <a className={classes.link} href={a.link}>
                                            <Grid container>
                                                <Grid item xs={1}>
                                                    <InsertDriveFileIcon style={{color: '#6497b1', fontSize: 40}}/>
                                                </Grid>
                                                <Grid item xs={11}>
                                                    <div className={classes.letritas}>{a.nombre}.{a.extension}</div>
                                                </Grid>
                                            </Grid>
                                        </a>
                                    </Grid>
                                    </AccordionSummary>
                                    <AccordionDetails>
                                    <Grid aling="center">
                                    
                                                <p>----------------- Properties -----------------</p>
                                                <Divider/>
                                                <p>Name: {datos[i].nombre}</p>
                                                <p>Extension: {datos[i].extension}</p>
                                                <p>Date: {datos[i].fecha}</p>
                                                <p>Code: {datos[i].codigo}</p>
                                                <p>Owner: {datos[i].propietario}</p>
                                                <p>Link: {datos[i].link}</p>
                                                <p>Folder: {datos[i].carpeta}</p>
                                                <Divider/>
                                    </Grid>
                                    </AccordionDetails>
                                </Accordion>
                            </div>
                        )
                    })}
                </Container>
            </div>
        </>
    )
}