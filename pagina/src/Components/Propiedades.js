import React, {useState} from "react";
import {makeStyles}  from '@material-ui/core/styles';
import {Grid, Button, Container, Avatar, Paper, TextField, Typography, Modal, Divider} from '@material-ui/core'

function getModalStyle(){
    return{
        top: '20%',
        left: '30%',
        transform: 'tanslate(-45%,-45%)',
    };
}

const useStyles = makeStyles((theme) => ({
    root: {
        position: 'relative',
        backgroundColor: "#b3cde0",
        paddingTop: '20px',
        paddingLeft: '100px',
        paddingRight: '100px',
        paddingBottom: '50px'
    },
    rootGrid: {
        flexGrow: 1
    },
    paperStyle: {
        padding: 20,
        height: "7vh",
        width: 1300,
        margin: "20px auto"
    },
    paperStyle2: {
        padding: 20,
        height: "80vh",
        width: 1300,
        margin: "20px auto"
    },
    btn: {
        margin: '1px 1px',
        backgroundColor: '#005b96',
        color: 'black',
        height: '25px',
        width: '25px'
    },
    avatar:{
        backgroundColor: '#6497b1',
        width: 60,
        height: 60
    },
    divider: {
        margin: theme.spacing(1, 0),
    },
    createFolder: {
        backgroundColor: '#6497b1',
        backgroundImage: '#3234ab'
    },
    letritas: {
        margin: '2px',
        padding: '6px',
        fontFamily: 'Times New Roman',
        fontSize: '20px'
    },
    paper:{
        position: 'absolute',
        width: 300,
        height: 350,
        backgroundColor: "#ffffff",
        border: '2px solid #000000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2,4,3),
    },
    divider: {
        margin: theme.spacing(1, 0),
    },
    link:{
        textDecoration: 'none',
        color: '#000000'
    },
    container:{
        backgroundColor: "#ffffff"
    }
  }));

export default function Propiedades (props){

    const classes = useStyles();

    const [modalStyle] = React.useState(getModalStyle);

    const [open2, setOpen2] = React.useState(false);

    const handleOpen2 =()=>{
        setOpen2(true);
        console.log(open2)
    };
    
    var o = props.open;
    console.log(o)
    const handleClose2 =()=>{
        o = false;
        console.log(o)
        setOpen2(false);
    };


    return(
        <Modal
            open={o}
            onClose={handleClose2}
        >
        <div style={modalStyle} className={classes.paper}>
            <p>Propiedades</p>
            <Divider/>
            <p>Name: {props.nombre}</p>
            <div>Fecha de creacion {props.fecha}</div>
            <Divider/>
            <Button onClick={handleClose2}>cerrar</Button>
        </div>
        </Modal>
    );
}