import React, {useState} from 'react'
import {Grid, Button, Link, Avatar, Paper, TextField, Typography} from '@material-ui/core'
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import {makeStyles}  from '@material-ui/core/styles';
import DateFnsUtils from '@date-io/date-fns';
import NavBar from './Navbar';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
  } from '@material-ui/pickers';

import axios from 'axios';
import Swal from 'sweetalert2';
import Barra from './Barra';

var md5 = require('md5');
const url = "http://34.71.225.235:3000/";

const useStyles = makeStyles((theme) => ({
    root: {
        position: 'relative',
        backgroundColor: "#b3cde0",
        paddingTop: '20px',
        paddingLeft: '100px',
        paddingRight: '100px',
        paddingBottom: '50px'
    },
    calendar: {
        color: "#676767"
    },
    paperStyle: {
        padding: 15,
        height: "80vh",
        width: 300,
        margin: "20px auto"
    },
    btn: {
        margin: '15px 0',
        backgroundColor: '#005b96',
        color: 'white'
    },
    avatar:{
        backgroundColor: '#005b96',
        width: 60,
        height: 60
    }
  }));

const Register = () =>{

    const [selectedDate, setSelectedDate] = React.useState(new Date('2021-08-18T21:11:54'));

    const handleDateChange = (date) => {
        setSelectedDate(date);
        //console.log("la fecha es: "+ selectedDate.getMonth())
    };
    
    const classes = useStyles();

    const [datos, setDatos] = useState({
        username: '',
        email: '',
        phone: '',
        pass: '',
        repass: '', 
        date: ''
    })

    const handleInputChange = (event) =>{

        setDatos({
            ...datos,
            [event.target.name]: event.target.value
        })
    }

    const registrar = (event) =>{
                
        if(datos.pass === datos.repass){
            registrando();
        }else(
            Swal.fire({
                icon: 'error',
                title: 'Error',
                showConfirmButton: false,
                timer: 2000
            })
        )
    }

    const registrando = async()=>{
        console.log("Celular: " + datos.phone)
        axios.post(url+"ddb/person/save", {"id": datos.username, "correo": datos.email, "celular": datos.phone,"password": md5(datos.pass), "fecha": selectedDate.getDate() + '/' + selectedDate.getMonth() + '/' + selectedDate.getFullYear()})
        .then(response=>{
            //respuesta
            var resp = { "RESPUESTA": "hola"};
            resp = response.data
            console.log(resp.RESPUESTA)
            if(resp.RESPUESTA === "EXITOSO/"){
                Swal.fire({
                    icon: 'success',
                    title: 'Success',
                    showConfirmButton: false,
                    timer: 2000
                })
                carpetaroot();
                save();
                crearPapelera();
                papelera();
                setTimeout("location.href='./login'", 2000);
            }else{
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    showConfirmButton: false,
                    timer: 2000
                })
            }
        })
        .catch(error=>{
            console.log(error);
        })
    }

    const carpetaroot = async()=>{
        axios.post(url+"s3/carpetaroot", {"name": datos.username})
        .then(response=>{
            var resp = { "RESPUESTA": "hola"};
            resp = response.data
            console.log(resp.RESPUESTA)
            if(resp.RESPUESTA === "SE CREO CARPETA ROOT/"){
                console.log("carpeta root creada")
            }else{
                console.log("carpeta no creada")
            }
        })
        .catch(error=>{
            console.log(error);
        })
    }

    const save = async()=>{
        var f = new Date();
        var fechaactual = f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear();
        axios.post(url+"ddb/carpeta/save",{nombre: datos.username, cantidad:"0", fecha: fechaactual, propietario: datos.username})
        .then(response=>{
            var resp = { "RESPUESTA": "hola"};
            resp = response.data
            console.log(resp.RESPUESTA)
            if(resp.RESPUESTA == "EXITOSO/"){
                console.log("carpeta root creada en bd")
            }else{
                console.log("carpeta no creada en bd")
            }
        })
        .catch(error=>{
            console.log(error);
        })
    }

    //papelera
    const crearPapelera = async()=>{
        axios.post(url+"s3/crearpapelera",{name: datos.username + '/'})
        .then(response=>{
            var resp = { "RESPUESTA": "hola"};
            resp = response.data
            console.log(resp.RESPUESTA)
            if(resp.RESPUESTA == "SE CREO CARPETA DE PAPELERA EN LA CARPETA ROOT/"){
                console.log("carpeta PAPELERA creada en S3")
            }else{
                console.log("carpeta PAPELERA no creada en S3")
            }
        })
        .catch(error=>{
            console.log(error);
        })
    }

    const papelera = async()=>{
        var f = new Date();
        var fechaactual = f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear();
        axios.post(url+"ddb/carpeta/save",{nombre: 'papelera', cantidad:"0", fecha: fechaactual, propietario: datos.username})
        .then(response=>{
            var resp = { "RESPUESTA": "hola"};
            resp = response.data
            console.log(resp.RESPUESTA)
            if(resp.RESPUESTA == "EXITOSO/"){
                console.log("carpeta papelera creada en bd")
            }else{
                console.log("carpeta papelera no creada en bd")
            }
        })
        .catch(error=>{
            console.log(error);
        })
    }

    return(
        <>
            <NavBar/>
            <div className={classes.root}>
                <Barra titulo="Sign Up"></Barra>
                <Grid>
                    <Paper elevation={10} className={classes.paperStyle}>
                        <Grid align="center">
                            <Avatar className={classes.avatar}> <AccountCircleIcon/> </Avatar>
                            <h2>Sign Up</h2>
                        </Grid>

                        <TextField onChange={handleInputChange} name="username" placeholder="Please enter an username." label="Username" fullWidth required></TextField>
                        <TextField onChange={handleInputChange} name="email" placeholder="Please enter an email." label="E-mail" type="email" fullWidth required></TextField>
                        <TextField onChange={handleInputChange} name="phone" placeholder="Please enter a phone." label="Phone" type="number" fullWidth required></TextField>
                        <TextField onChange={handleInputChange} name="pass"placeholder="Please enter password." label="Password"  type="password" fullWidth required></TextField>
                        <TextField onChange={handleInputChange} name="repass"placeholder="Confirm the password." label="Confirm Pass"  type="password" fullWidth required></TextField>

                        <MuiPickersUtilsProvider utils={DateFnsUtils}  className={classes.calendar}>
                        <Grid container justifyContent="space-around">
                            <KeyboardDatePicker
                            margin="normal"
                            id="date-picker-dialog"
                            label="Birth Date"
                            format="dd/MM/yyyy"
                            value={selectedDate}
                            onChange={handleDateChange}
                            KeyboardButtonProps={{
                                'aria-label': 'change date',
                            }}
                            />
                        </Grid>
                        </MuiPickersUtilsProvider>

                        <Button onClick={registrar} className={classes.btn} type="submit"  variant="contained" fullWidth>Sign up</Button>
                        <Grid align="center">
                        <Typography> ¿You have an account? </Typography>
                        <Link href="./signup">
                                Login
                        </Link>
                        </Grid>
                    </Paper>
                </Grid>
            </div>
        </>
    )
}

export default Register