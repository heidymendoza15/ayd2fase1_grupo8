import React, {useState} from 'react'
import {Grid, Button, Link, Avatar, Paper, TextField, Typography} from '@material-ui/core'
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import {makeStyles}  from '@material-ui/core/styles';
import NavBar from './Navbar';

import axios from 'axios';
import Swal from 'sweetalert2';
import Cookies from 'universal-cookie';
import Barra from './Barra';
 
const cookies = new Cookies();

const url = "http://34.71.225.235:3000/";
const urlFlask = "http://52.15.205.35:3000/ddb/person/login"

var md5 = require('md5');

const useStyles = makeStyles((theme) => ({
    root: {
        position: 'relative',
        backgroundColor: "#b3cde0",
        paddingTop: '20px',
        paddingLeft: '100px',
        paddingRight: '100px',
        paddingBottom: '50px'
    },
    imagen: {
        width: '475px'
    },
    divider: {
        margin: theme.spacing(1, 0),
    },
    paperStyle: {
        padding: 20,
        height: "70vh",
        width: 290,
        margin: "20px auto"
    },
    btn: {
        margin: '15px 0',
        backgroundColor: '#005b96',
        color: 'white'
    },
    avatar:{
        backgroundColor: '#6497b1',
        width: 60,
        height: 60
    }
  }));

const Login = () =>{
    
    const classes = useStyles();

    const [datos, setDatos] = useState({
        username: '',
        pass: ''
    })

    const handleInputChange = (event) =>{
        
        setDatos({
            ...datos,
            [event.target.name]: event.target.value
        })
    }

    const enviarDatos = (event) =>{
        console.log(datos)
        entrar();
    }

    //{servicio: "usuario", accion: "login", campos: {id: datos.username, password: datos.pass}}
    const entrar = async()=>{
        console.log("ESTE ES EL VERDADERO LOGIN")
        axios.post(url+"ddb/person/login",{"id": datos.username, "password": md5(datos.pass)})
        .then(response=>{
            var JsonObj = response.data; 
            if(JsonObj.hasOwnProperty("auth")){
                console.log("Tiene auth");
                var resp = { "RESPUESTA": "hola"};
                if(JsonObj.hasOwnProperty("auth")){
                    Swal.fire({
                        icon: 'warning',
                        title: 'Authentication',
                        showConfirmButton: false,
                        timer: 2000
                    })
                    
                    axios.post(url+"ddb/person/perfil",{"id": datos.username})
                    .then(response=>{

                        console.log(response.data)
                        //guardar datos usuario en coockies
                        cookies.set('name', response.data[0].id, {path: '/'});
                        cookies.set('email', response.data[0].correo, {path: '/'});
                        cookies.set('date', response.data[0].fecha, {path: '/'});
                        cookies.set('carpetaA', response.data[0].id, {path: '/'});
                        cookies.set('pass', response.data[0].password, {path: '/'});
                        cookies.set('auth', response.data[0].aut, {path: '/'});
                        //console.log(cookies.get('pass'), cookies.get('email'), cookies.get('date')); //prueba
                        setTimeout("location.href='./login_token'", 500);
                    }).catch(error=>{
                        console.log(error);
                    })   
                }
                
            }else{
                console.log("No tiene autenticacion");
                console.log(response.data);
                var resp = { "RESPUESTA": "hola"};
                console.log(resp.RESPUESTA)
                resp = response.data
                console.log(resp.RESPUESTA)
                if(resp.RESPUESTA == "EXITOSO/"){
                    Swal.fire({
                        icon: 'success',
                        title: 'Welcome',
                        showConfirmButton: false,
                        timer: 2000
                    })
                    
                    axios.post(url+"ddb/person/perfil", {"id": datos.username})
                    .then(response=>{

                        console.log(response.data)
                        //guardar datos usuario en coockies
                        cookies.set('name', response.data[0].id, {path: '/'});
                        cookies.set('email', response.data[0].correo, {path: '/'});
                        cookies.set('date', response.data[0].fecha, {path: '/'});
                        cookies.set('carpetaA', response.data[0].id, {path: '/'});
                        cookies.set('pass', response.data[0].password, {path: '/'});
                        //console.log(cookies.get('pass'), cookies.get('email'), cookies.get('date')); //prueba
                        setTimeout("location.href='./profile'", 500);
                    }).catch(error=>{
                        console.log(error);
                    })

                    
                }else{
                    Swal.fire({
                        icon: 'error',
                        title: 'Error',
                        showConfirmButton: false,
                        timer: 2000
                    })
                }
            }
        })
        .catch(error=>{
            console.log(error);
        })
    }

    
    /*const entrar = async()=>{
        axios.post(urlFlask, {"id": datos.username, "password": md5(datos.pass)})
        .then(response=>{
            console.log(response.data);
            
            var resp = { "RESPUESTA": "hola"};
            //console.log(resp.RESPUESTA)
            resp = response.data
            //console.log(resp.RESPUESTA)
            if(resp.RESPUESTA == "EXITOSO/"){
                Swal.fire({
                    icon: 'success',
                    title: 'Welcome',
                    showConfirmButton: false,
                    timer: 2000
                })
                //console.log(resp)
                
                axios.post(url,{servicio: "usuario", accion: "perfil", campos: {"id": datos.username}})
                .then(response=>{

                    console.log(response.data)
                    //guardar datos usuario en coockies
                    cookies.set('name', response.data[0].id, {path: '/'});
                    cookies.set('email', response.data[0].correo, {path: '/'});
                    cookies.set('date', response.data[0].fecha, {path: '/'});
                    cookies.set('carpetaA', response.data[0].id, {path: '/'});
                    cookies.set('pass', response.data[0].password, {path: '/'});
                    //console.log(cookies.get('pass'), cookies.get('email'), cookies.get('date')); //prueba
                    setTimeout("location.href='./profile'", 500);
                }).catch(error=>{
                    console.log(error);
                })

                
            }else{
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    showConfirmButton: false,
                    timer: 2000
                })
            }
        })
        .catch(error=>{
            console.log(error);
        })
    }*/

    return(
        <>
            <NavBar/>
            <div className={classes.root}>
                <Barra titulo="Login"></Barra>
                <Grid>
                    <Paper elevation={10} className={classes.paperStyle}>
                        <Grid align="center">
                            <Avatar className={classes.avatar}> <AccountCircleIcon/> </Avatar>
                            <h2>Login</h2>
                        </Grid>

                        <TextField onChange={handleInputChange} name="username" placeholder="Please enter username." label="Username" fullWidth required></TextField>
                        <TextField onChange={handleInputChange} name="pass"placeholder="Please enter password." label="Password"  type="password" fullWidth required></TextField>
                        <Button onClick={enviarDatos} className={classes.btn} type="submit"  variant="contained" fullWidth>Login</Button>
                        <Grid align="center">
                        <Typography> ¿You haven't an account? </Typography>
                        <Link href="./signup">
                            Sign up now
                        </Link>
                        </Grid>
                    </Paper>
                </Grid>
            </div>
        </>
    )
}

export default Login