import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Cookies from 'universal-cookie';

const cookies = new Cookies();
const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(10),
    },
    title: {
      flexGrow: 1,
      color: 'white',
    },
    menu:{
      background: '#005b96'
    },
    boton:{
      color: 'white',
      fontSize: '14px'
    },
    itemMenu:{
        textDecoration: 'none',
        color: '#000000'
    }
  }));

  
function NavBar2() {

  const CerrarSesion=()=>{
    cookies.remove('name', {path: "/"});
    cookies.remove('email', {path: "/"});
    cookies.remove('date', {path: "/"});
    cookies.remove('pass', {path: "/"});
    window.location.href='./';
  }

  const botonPerfil=()=>{
    window.location.href='/profile';
  }

  const botonUpdate=()=>{
    window.location.href='/edit_profile';
  }

  const botonPapelera=()=>{
    window.location.href='/papelera';
  }
  
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const classes = useStyles();
  
    return (
      <div>
        <AppBar position="static" className={classes.menu}>
            <Toolbar variant="dense">
                <Typography variant="h4" className={classes.title}>
                AyDrive!
                </Typography>
            
                <Button color="inherit" className={classes.boton} onClick={()=>{window.location.href='/aydrive';}}>My Drive</Button>
                <Button color="inherit" className={classes.boton} aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
                My Profile
                </Button>
                <Menu
                  id="simple-menu"
                  anchorEl={anchorEl}
                  keepMounted
                  open={Boolean(anchorEl)}
                  onClose={handleClose}
                >
                  <MenuItem onClick={handleClose}><a classname={classes.itemMenu}  onClick={botonPerfil}>Profile</a></MenuItem>
                  <MenuItem onClick={handleClose}><a classname={classes.itemMenu} onClick={botonUpdate}>Update Profile</a></MenuItem>
                  <MenuItem onClick={handleClose}><a classname={classes.itemMenu}  onClick={CerrarSesion}>Logout</a></MenuItem>
                  <MenuItem onClick={handleClose}><a classname={classes.itemMenu}  onClick={botonPapelera}>Recycle Bin</a></MenuItem>
                </Menu>
            </Toolbar>
        </AppBar> 
      </div>
    )
  }
  
export default NavBar2