import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Barra from './Barra';

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(10),
    },
    title: {
      flexGrow: 1,
      color: 'white',
    },
    menu:{
      background: '#005b96'
    },
    boton:{
      color: 'white',
      fontSize: '14px'
    }
  }));

function NavBar() {

    const classes = useStyles();
  
    return (
      <div>
        <AppBar position="static" className={classes.menu}>
            <Toolbar variant="dense">
                <Typography variant="h4" className={classes.title}>
                AyDrive!
                </Typography>
           
                <Button color="inherit" className={classes.boton} onClick={()=>{window.location.href='./login';}}>Login</Button>
                <Button color="inherit" className={classes.boton} onClick={()=>{window.location.href='./signup';}}>Sign Up</Button>
            </Toolbar>
        </AppBar> 
      </div>
    )
  }
  
  export default NavBar