import {React, useEffect, useState} from 'react';
import NavBar2 from '../Components/NavBar2';
import {makeStyles}  from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid'
import {Avatar, Paper, Button, Divider, TextField, Typography} from '@material-ui/core';
import Cookies from 'universal-cookie';
import axios from 'axios';

const cookies = new Cookies();
var url = "http://34.71.225.235:3000/"

const useStyles = makeStyles((theme) => ({
    root: {
        position: 'relative',
        backgroundColor: "#b3cde0",
        paddingTop: '20px',
        paddingLeft: '100px',
        paddingRight: '100px',
        paddingBottom: '30px'
    },
    divider: {
        margin: theme.spacing(3,10),
    },
    divider2: {
        margin: theme.spacing(2,25),
    },
    paper: {
        height: '85vh',
        width: 1000,
        margin: "20px auto",
        textAlign: 'center'
    },
    small: {
        backgroundColor: "#011f4b",
        width: theme.spacing(3),
        height: theme.spacing(3),
      },
    large: {
        backgroundColor: "#011f4b",
        borderColor: "#011f4b",
        width: theme.spacing(16),
        height: theme.spacing(16),
    },
    texto:{
        color: "#011f4b",
    },
    btn:{
        backgroundColor:"#03396c",
        margin: "20px"
    }
  }));



export default function Profile (){

    
    const [dato, setDato] = useState('')
    const [autenticar, setAutenticar] = useState('autenticar');

    useEffect(() => {
        if(!cookies.get('name')){
            window.location.href='./';
        }

        axios.post(url+"ddb/person/perfil",{"id": cookies.get('name')})
        .then(response=>{
            console.log(response.data)
            cookies.set('name', response.data[0].id, {path: '/'});
            cookies.set('email', response.data[0].correo, {path: '/'});
            cookies.set('date', response.data[0].fecha, {path: '/'});
            cookies.set('phone', response.data[0].celular, {path: '/'});
            cookies.set('pass', response.data[0].password, {path: '/'});
            cookies.set('auth', response.data[0].aut, {path: '/'});
            setDato(response.data[0].id)
        }).catch(error=>{
            console.log(error);
        })

        if(cookies.get('auth') == 'si'){
            setAutenticar('Activated');
        }else{
            setAutenticar('Desactivated');
        }
    },[]);

    var perfil = '@'+cookies.get('name');
    var email = cookies.get('email');
    var date = cookies.get('date');
    var auth = autenticar;
    var cel = cookies.get('phone');

    const classes = useStyles();
        return ( 
            <>
            <NavBar2/>   
                <div className={classes.root}>
                <Grid>
                    <Paper elevation={10} className={classes.paper}>
                        <Grid align="center">
                            <h1>My Profile</h1>
                            <Avatar className={classes.large} alt="Remy Sharp" src="https://thumbs.gfycat.com/IlliterateUntidyAmericanbulldog-size_restricted.gif" />
                            <h2>{perfil}</h2>
                        </Grid>

                        <Divider className={classes.divider}/>

                        <Grid align="center">

                            <Grid item xs={6} align="center">
                                <Typography variant="h5" color="initial" align="left"><b>Username:</b> { perfil } </Typography>
                                <Typography variant="h5" color="initial" align="left"><b>E-mail:</b> { email } </Typography>
                                <Typography variant="h5" color="initial" align="left"><b>Phone:</b> { cel } </Typography>
                                <Typography variant="h5" color="initial" align="left"><b>Birth Date:</b> { date } </Typography>
                                <Typography variant="h5" color="initial" align="left"><b>Authentication:</b> { auth } </Typography>
                            </Grid>


                            <Divider className={classes.divider2}/>

                            <Grid item xs={6}> 
                            <Button className={classes.btn} type="submit" color="primary" variant="contained" href="./edit_profile">Update Data</Button>
                            <Button className={classes.btn} type="submit" color="primary" variant="contained" href="./aydrive" >My Drive</Button>
                            </Grid>

                             

                        </Grid>
                    </Paper>
    
                </Grid>
                </div>
            </>
        );
}
