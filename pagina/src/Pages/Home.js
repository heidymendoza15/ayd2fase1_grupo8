import React from 'react';
import NavBar from '../Components/Navbar';
import {makeStyles}  from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid'
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography'
import Divider from '@material-ui/core/Divider'
import Barra from '../Components/Barra';

const useStyles = makeStyles((theme) => ({
    root: {
        position: 'relative',
        backgroundColor: "#b3cde0",
        paddingTop: '20px',
        paddingLeft: '100px',
        paddingRight: '100px',
        paddingBottom: '30px'
    },
    imagen: {
        width: '475px'
    },
    divider: {
        margin: theme.spacing(1, 0),
    },
    paper: {
        height: '70vh',
        width: 280,
        margin: "20px auto"
      }
  }));

  const Home = () =>{

    const classes = useStyles();
        return ( 
            <>
            <NavBar/>   
                <div className={classes.root}>
                    <Grid>
                        <Barra titulo="Home"></Barra>
                        <Paper elevation={10} className={classes.paperStyle}>
                        <Typography variant="h3" align="center" color="textPrimary">Calificacion Fase 3</Typography>
        
                        <Divider className={classes.divider} />
                            <Grid align="center">
                            <img className={classes.imagen} src="https://static.vecteezy.com/system/resources/previews/002/774/592/non_2x/web-page-design-illustration-concept-vector.jpg"></img>
                            </Grid>
                        </Paper>
                    </Grid>
                </div>
            </>
        );
}
 
export default Home;
