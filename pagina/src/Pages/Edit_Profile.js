import {React, useState, useEffect} from 'react';
import NavBar2 from '../Components/NavBar2';
import {makeStyles}  from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid'
import {Avatar, Paper, Button,FormGroup, FormControlLabel, Divider, TextField, Typography} from '@material-ui/core';
import Cookies from 'universal-cookie';
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';
import Checkbox from '@material-ui/core/Checkbox';

import axios from 'axios';
import Swal from 'sweetalert2';

var md5 = require('md5');
const cookies = new Cookies();

var url = "http://34.71.225.235:3000/"

const useStyles = makeStyles((theme) => ({
    root: {
        position: 'relative',
        backgroundColor: "#b3cde0",
        paddingTop: '20px',
        paddingLeft: '100px',
        paddingRight: '100px',
        paddingBottom: '30px'
    },
    divider: {
        margin: theme.spacing(3,10),
    },
    divider2: {
        margin: theme.spacing(2,25),
    },
    paper: {
        height: '105vh',
        width: 1000,
        margin: "20px auto",
        textAlign: 'center'
    },
    small: {
        backgroundColor: "#011f4b",
        width: theme.spacing(3),
        height: theme.spacing(3),
      },
    large: {
        backgroundColor: "#011f4b",
        borderColor: "#011f4b",
        width: theme.spacing(16),
        height: theme.spacing(16),
    },
    texto:{
        color: "#011f4b",
    },
    btn:{
        backgroundColor:"#03396c",
        margin: "20px"
    }
  }));


  var perfil = '@'+cookies.get('name');
  var emailC = cookies.get('email');
  var dateC = cookies.get('date');
  var passC = cookies.get('pass');
  var aut = cookies.get('auth')

export default function Edit_Profile () {

    useEffect(() => {
        if(!cookies.get('name')){
            window.location.href='./';
        }

        if(aut == "si"){
            console.log("tengo autenticacion");
            setCheckedE('Disable');
        }else{
            console.log("no tengo autenticacion");
        }
    },[]);

    //UseState para check de autenticacion
    const [checked, setCheckedE] = useState('Enable');

    const [selectedDate, setSelectedDate] = useState(new Date('1900-01-01T21:11:54'));

    const handleDateChange = (date) => {
        setSelectedDate(date);
        //console.log("FECHA:"+selectedDate.getDay()+" "+selectedDate.getMonth())
    };

    const [datos, setDatos] = useState({
        email: '',
        pass: '',
        date: ''
    })
    
    const handleInputChange = (event) =>{
    
        setDatos({
            ...datos,
            [event.target.name]: event.target.value
        })
    }
    
    var fechaUpdate = "";
    const actualizar = (event) =>{
                 
        if( datos.email === "" )
        {
            datos.email = emailC;

        }
        
        var pass5 = md5(datos.pass);
        if( datos.pass === ""){
            datos.pass = passC;
        }else{
            datos.pass = pass5;
        }

        var aa = selectedDate.getFullYear();
        var dd = selectedDate.getDate();
        var mm = selectedDate.getMonth()+1;
        if(aa === 1900 && dd === 1 && mm === 1 ){
            fechaUpdate = dateC;
        }else{
            fechaUpdate =  selectedDate.getDate() + '/' + mm + '/' + selectedDate.getFullYear();
            console.log(dd+" "+mm+" "+aa)
        }
        actualizando();
        console.log(datos)
    }
    
    const actualizando   = async()=>{
        //console.log("fecha: "+ selectedDate.getDay() + '/' + mm + '/' + selectedDate.getFullYear())
        axios.put(url+"ddb/usuario/update", {"id": cookies.get('name'), "correo": datos.email, "password": datos.pass, "fecha": fechaUpdate})
        .then(response=>{
            //respuesta
            var resp = { "RESPUESTA": "hola"};
            resp = response.data
            console.log(resp.RESPUESTA)
            if(resp.RESPUESTA === "EXITOSO/"){
                Swal.fire({
                    icon: 'success',
                    title: 'Success',
                    showConfirmButton: false,
                    timer: 2000
                })
                setTimeout("location.href='./profile'", 2000);
            }else{
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    showConfirmButton: false,
                    timer: 2000
                })
            }
        })
        .catch(error=>{
            console.log(error);
        })
    }

    var authRes = "";
    const actualizarAut = (event) =>{
        console.log("cambiar autenticacion")
        if(checked === "Enable"){
            authRes = "si";
        }else{
            authRes = "no";
        }  
        actualizandoAut();  
    }

    const actualizandoAut   = async()=>{
        console.log("entra ?");
        axios.put(url+"ddb/usuario/updateAuth",{"id": cookies.get('name'), "auth": authRes})
        .then(response=>{
            //respuesta
            var resp = { "RESPUESTA": "hola"};
            resp = response.data
            console.log(resp.RESPUESTA)
            if(resp.RESPUESTA === "EXITOSO/"){
                Swal.fire({
                    icon: 'success',
                    title: 'Updated',
                    showConfirmButton: false,
                    timer: 2000
                })
                setTimeout("location.href='./profile'", 3000);
            }else{
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    showConfirmButton: false,
                    timer: 2000
                })
            }
        })
        .catch(error=>{
            console.log(error);
        })
    }

    const classes = useStyles();
        return ( 
            <>
            <NavBar2/>   
                <div className={classes.root}>
                <Grid>
                    <Paper elevation={10} className={classes.paper}>
                        <Grid align="center">
                            <h1>Edit My Profile</h1>
                            <Avatar className={classes.large} alt="Remy Sharp" src="https://media4.giphy.com/media/fVevyFHZOmlRtiqugu/giphy.gif?cid=790b7611f18c3d886052fc4885779c3a516596b6d8791499&rid=giphy.gif&ct=g" />
                            <h2>{perfil}</h2>
                        </Grid>

                        <Divider className={classes.divider}/>

                        <Grid align="center">

                            <Grid item xs={6} >
                                <TextField onChange={handleInputChange} name="email" placeholder="Please enter a new email." label="New E-mail" type="email" fullWidth required></TextField>
                                <TextField onChange={handleInputChange} name="pass"placeholder="Please enter a new password." label=" New Password"  type="password" fullWidth required></TextField> 
                                <MuiPickersUtilsProvider utils={DateFnsUtils}  className={classes.calendar}>
                                <Grid container justifyContent="space-around">
                                    <KeyboardDatePicker
                                    margin="normal"
                                    id="date-picker-dialog"
                                    label="New Birth Date"
                                    format="dd/MM/yyyy"
                                    value={selectedDate}
                                    onChange={handleDateChange}
                                    KeyboardButtonProps={{
                                        'aria-label': 'change date',
                                    }}
                                    />
                                </Grid>
                                </MuiPickersUtilsProvider>                             
                            </Grid>


                            <Divider className={classes.divider2}/>

                            <Grid item xs={6}> 
                            <Button  onClick={actualizar} className={classes.btn} type="submit" color="primary" variant="contained" type="submit">Update</Button>
                            <Button  onClick={actualizarAut}  type="submit" color="primary" variant="contained" >{checked} Authentication</Button>
                            <Button className={classes.btn} type="submit" color="primary" variant="contained" href="./profile" >Cancel</Button>
                            </Grid>
                        </Grid>
                    </Paper>
    
                </Grid>
                </div>
            </>
        );
}
 
