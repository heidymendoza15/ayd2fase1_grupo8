import React, {useState, useEffect} from 'react'
import NoteAddIcon from '@material-ui/icons/NoteAdd';
import DeleteIcon from '@material-ui/icons/Delete';
import UpdateIcon from '@material-ui/icons/Update';
import SwapHorizontalCircleIcon from '@material-ui/icons/SwapHorizontalCircle';
import InsertDriveFileIcon from '@material-ui/icons/InsertDriveFile';


import {Grid, Button, Container, Paper, TextField, Modal, Divider, Accordion, AccordionDetails, AccordionSummary, ListItemSecondaryAction, InputLabel, MenuItem, FormHelperText, FormControl, Select, Typography} from '@material-ui/core'
import {makeStyles}  from '@material-ui/core/styles';
import NavBar2 from '../Components/NavBar2';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Tooltip from '@material-ui/core/Tooltip';
  
import axios from 'axios';
import Swal from 'sweetalert2';
import Cookies from 'universal-cookie';
import { useParams } from 'react-router';
 
const cookies = new Cookies();

const url = "http://34.71.225.235:3000/";

function getModalStyle(){
    return{
        top: '20%',
        left: '30%',
        transform: 'tanslate(-45%,-45%)',
    };
}

const useStyles = makeStyles((theme) => ({
    root: {
        position: 'relative',
        backgroundColor: "#b3cde0",
        paddingTop: '20px',
        paddingLeft: '100px',
        paddingRight: '100px',
        paddingBottom: '50px'
    },
    custom: {
        paddingTop: "25px",
        content: 'Select some files',
        display: "inline-block",
        padding: "15px 50px",
        outline: "none",
    },
    rootGrid: {
        flexGrow: 1
    },
    paperStyle: {
        padding: 20,
        height: "7vh",
        width: 1300,
        margin: "20px auto"
    },
    paperStyle2: {
        padding: 20,
        height: "80vh",
        width: 1300,
        margin: "20px auto"
    },
    btn: {
        margin: '1px 1px',
        backgroundColor: '#005b96',
        color: 'white',
        height: '50px',
        width: '50px'
    },
    divider: {
        margin: theme.spacing(1, 0),
    },
    createFolder: {
        backgroundColor: '#6497b1',
        backgroundImage: '#3234ab'
    },
    letritas: {
        margin: '2px',
        padding: '6px',
        fontFamily: 'Times New Roman',
        fontSize: '20px'
    },
    paper:{
        position: 'absolute',
        width: 400,
        backgroundColor: "#ffffff",
        border: '2px solid #000000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2,4,3),
    },
    divider: {
        margin: theme.spacing(1, 0),
    },
    divider2: {
        margin: theme.spacing(2, 5),
    },
    link:{
        textDecoration: 'none',
        color: '#000000'
    },
    container:{
        backgroundColor: "#ffffff"
    },
    rootAcordeon: {
        width: '100%',
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        flexBasis: '33.33%',
        flexShrink: 0,
    },
    secondaryHeading: {
        fontSize: theme.typography.pxToRem(15),
        color: theme.palette.text.secondary,
      },
    formControl: {
        minWidth: 400,
        position: 'relative',
        backgroundColor: "white",
        fontSize: 16,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
      },
  }));

export default function V_folder () {

    const { name } = useParams()
    console.log(name)


    //eliminar
    const [openEliminarArchivo, setOpenEliminarArchivo] = React.useState(false);

    const abrirEliminarArchivo =()=>{
        setOpenEliminarArchivo(true);
    };

    const cerrarEliminarArchivo =()=>{
        setOpenEliminarArchivo(false);
    };

    const eliminarArchivo=()=>{
        console.log("mover con codigo: " + file + " a la papelera")
        var datosArchivo;

        axios.post(url+"ddb/archivo/ver",{codigo: file})
        .then(response=>{
            datosArchivo = response.data[0]
            //setArchivoDatos(response.data)
            
            var direccionV = cookies.get("name") + "/" + name + "/" + datosArchivo.nombre+"."+datosArchivo.extension;
            var direccionN = cookies.get("name") + "/papelera/" + datosArchivo.nombre+"."+datosArchivo.extension;
            console.log(direccionV)
            console.log(direccionN)
            //movemos archivo S3
            axios.post(url+"s3/moverlosarchivos",{direccionvieja: direccionV, direccionnueva: direccionN})
            .then(response=>{
                var resp = { "RESPUESTA": "hola"};
                resp = response.data
                console.log(resp)
                if(resp.RESPUESTA == "SI SE MOVIO DE LUGAR/"){
                    console.log("Si se movio el archivo en s3 mover en base de datos")
                    //setTimeout(`location.href='./${name}'`, 1000);
                }else{
                    console.log("archivo no se movio")
                }
            })
            .catch(error=>{
                console.log(error)
            })
            //movemos en base de datos
            //let URL = ''
            //concatenar la carpetaA/nombre.extension
            //        axios.post(url,{servicio: "s3", accion: "obtenerURL", campos: {name: archivoNombre.name + '.' + ext, dire: cookies.get("name")+"/"+name+ '/'}})
            /*    axios.post(url,{servicio: "s3", accion: "obtenerURL", campos: {name: datosArchivo.name + '.' + datosArchivo.extension, dire: cookies.get("name") + '/papelera/'}})
                .then(response=>{
                    var resp = { "Direccion": "hola"};
                    resp = response.data
                    console.log(resp.Direccion)
                    URL = resp.Direccion
                })
                .catch(error=>{
                    console.log(error);
                })
*/
            axios.put(url+"ddb/archivo/update",{codigo: file, propietario: cookies.get("name"), nombre: name + "/" + datosArchivo.nombre, carpeta: "papelera", link: datosArchivo.link})
            .then(response=>{
                var resp = { "RESPUESTA": "hola"};
                resp = response.data
                console.log(resp)
                if(resp.RESPUESTA == "EXITOSO/"){
                    console.log("SE MOVIO EL ARCHIVO")
                    setTimeout(`location.href='./${name}'`, 1000);
                }else{
                    console.log("archivo no se movio")
                }
            })
            .catch(error=>{
                console.log(error)
            })            
        })
        .catch(error=>{
            console.log(error)
        })

        
        /*console.log("eliminar archivo con codigo: " + file)
        axios.post(url,{servicio: "archivo", accion: "delete", campos:{codigo: file, propietario: cookies.get("name")}})
        .then(response=>{
            var resp = { "RESPUESTA": "hola"};
            resp = response.data
            console.log(resp)
            if(resp.RESPUESTA == "EXITOSO/"){
                console.log("archivo eliminado")
                setTimeout(`location.href='./${name}'`, 1000);
            }else{
                console.log("archivo no eliminado")
                setTimeout(`location.href='./${name}'`, 1000);
            }
        })
        .catch(error=>{
            console.log(error)
        })*/
    }    
    /******** */

    //update archivo
    const [archivoUpdate, setarchivoUpdate] = React.useState('');
    const seleccionarArchivoUpdate = (event) =>{
        setarchivoUpdate(event.target.value)
    }

    const [nuevoNombre, setnuevoNombre] = useState({
        name: ''
    })

    const handleInputChangeNuevoNombre = (event) =>{
        
        setnuevoNombre({
            ...nuevoNombre,
            [event.target.name]: event.target.value
        })
    }



    const updateArchivo=()=>{
        console.log("Nuevo Nombre " + nuevoNombre.name + " al archivo: " + archivoUpdate )
       var datosArchivo;

       //buscamos el archivo
        axios.post(url+"ddb/archivo/ver",{codigo: archivoUpdate})
        .then(response=>{
            datosArchivo = response.data[0]
            console.log(datosArchivo)
            
            var nombreV = cookies.get("name") + "/" + name + "/" + datosArchivo.nombre+"."+datosArchivo.extension;
            var nombreN = cookies.get("name") + "/" + name + "/" + nuevoNombre.name+"."+datosArchivo.extension;

            console.log(nombreV);
            console.log(nombreN);
            //update archivo S3
            axios.post(url+"s3/renamefileTODO",{direccionviejonombre: nombreV, direccionnuevonombre: nombreN})
            .then(response=>{
                var resp = { "RESPUESTA": "hola"};
                resp = response.data
                console.log(resp)
                if(resp.RESPUESTA == "SI SE RENOMBRO/"){
                    console.log("Si se cambio el nombre en s3")
                    //setTimeout(`location.href='./${name}'`, 1000);
                }else{
                    console.log("archivo no se renombro")
                }
            })
            .catch(error=>{
                console.log(error)
            })
            //update en base de datos
            axios.put(url+"ddb/archivo/update",{codigo: archivoUpdate, propietario: cookies.get("name"), nombre: nuevoNombre.name, carpeta: name, link: datosArchivo.link})
            .then(response=>{
                var resp = { "RESPUESTA": "hola"};
                resp = response.data
                console.log(resp)
                if(resp.RESPUESTA == "EXITOSO/"){
                    console.log("SE RENOMBRO EL ARCHVIO")
                    setTimeout(`location.href='./${name}'`, 1000);
                }else{
                    console.log("********** NO SE RENOMBRO")
                }
            })
            .catch(error=>{
                console.log(error)
            })         
        })
        .catch(error=>{
            console.log(error)
        })
    }    

    //******************

    //mover archivo
    const [openMoverArchivo, setOpenMoverArchivo] = useState(false);

    const abrirMoverArchivo =()=>{
        setOpenMoverArchivo(true);
    };

    const cerrarMoverArchivo =()=>{
        setOpenMoverArchivo(false);
    };

    const moverArchivo=()=>{
        console.log("mover con codigo: " + ArchivoMover + " a la carpeta: " + Carpeta2 )
        var datosArchivo;

        axios.post(url+"ddb/archivo/ver",{codigo: ArchivoMover})
        .then(response=>{
            datosArchivo = response.data[0]
            //setArchivoDatos(response.data)
            
            var direccionV = cookies.get("name") + "/" + name + "/" + datosArchivo.nombre+"."+datosArchivo.extension;
            var direccionN = cookies.get("name") + "/" + Carpeta2 + "/" + datosArchivo.nombre+"."+datosArchivo.extension;
            console.log(direccionV)
            console.log(direccionN)
            //movemos archivo S3
            axios.post(url+"s3/moverlosarchivos",{direccionvieja: direccionV, direccionnueva: direccionN})
            .then(response=>{
                var resp = { "RESPUESTA": "hola"};
                resp = response.data
                console.log(resp)
                if(resp.RESPUESTA == "SI SE MOVIO DE LUGAR/"){
                    console.log("Si se movio el archivo en s3 mover en base de datos")
                    //setTimeout(`location.href='./${name}'`, 1000);
                }else{
                    console.log("archivo no se movio")
                }
            })
            .catch(error=>{
                console.log(error)
            })
            //movemos en base de datos
            axios.put(url+"ddb/archivo/update",{codigo: ArchivoMover, propietario: cookies.get("name"), nombre: datosArchivo.nombre, carpeta: Carpeta2, link: datosArchivo.link})
            .then(response=>{
                var resp = { "RESPUESTA": "hola"};
                resp = response.data
                console.log(resp)
                if(resp.RESPUESTA == "EXITOSO/"){
                    console.log("SE MOVIO EL ARCHIVO")
                    setTimeout(`location.href='./${name}'`, 1000);
                }else{
                    console.log("archivo no se movio")
                }
            })
            .catch(error=>{
                console.log(error)
            })            
        })
        .catch(error=>{
            console.log(error)
        })

        
    }    

    const [ArchivoMover, setArchivoMover] = useState('');

    const seleccionarArchivoMover = (event) =>{
        setArchivoMover(event.target.value)
    }

    const [Carpeta2, setCarpeta2] = useState('');

    const seleccionarCarpeta2 = (event) =>{
        setCarpeta2(event.target.value)
    }
    /******** */

    const [file, setFile] = React.useState('');

    const seleccionarArchivo = (event) =>{
        setFile(event.target.value)
        console.log("file " + file);
    }

    const classes = useStyles();

    //archivos
    const [datos2, setDatos2] = useState([]);
    
    const [modalStyle] = React.useState(getModalStyle);

    const [open3, setOpen3] = React.useState(false);
    const [open4, setOpen4] = React.useState(false);

    const [archivoNombre, setArchivoNombre] = useState({
        name: ''
    })
    
    //Para modales
    const handleOpen3 =()=>{
        setOpen3(true);
    };

    const handleClose3=()=>{
        setOpen3(false);
    };

    const handleOpenActualizar =()=>{
        setOpen4(true);
    };

    const handleCloseActualizar=()=>{
        setOpen4(false);
    };


    //carpetas
    const [datos, setDatos] = useState([]);
    //Archivos
    const [expanded, setExpanded] = React.useState(false);

    const handleChange = (panel) => (event, isExpanded) => {
        setExpanded(isExpanded ? panel : false);
    };

    useEffect(() => {
        //obtener carpetas
        //getCarpetas();
        axios.post(url+"ddb/carpeta/all",{propietario: cookies.get("name")})
        .then(response=>{
            console.log(response.data)
            setDatos(response.data);
            console.log(datos)
        })
        .catch(error=>{
            console.log(error);
        })

        //obtener archivos
        axios.post(url+"ddb/archivo/all",{propietario: cookies.get("name")})
        .then(response=>{
            let todo = response.data;
            let limpio = []
            for(let i = 0; i < todo.length; i++){
                if(todo[i].carpeta === name){// cookies = nueva .carpeta = nueva
                    limpio.push(todo[i])
                }
            }
            setDatos2(limpio);
            console.log("archivos")
            console.log(datos2)
        })
        .catch(error=>{
            console.log(error);
        })
    },[]);

    const handleInputChangeArchivo = (event) =>{
        
        setArchivoNombre({
            ...archivoNombre,
            [event.target.name]: event.target.value
        })
    }

    /*****************************Archivos */
    let enBase64 = '';
    let ext = '';
    const convertirBase64=(archivos)=>{
        Array.from(archivos).forEach(archivo=>{
            var reader = new FileReader();
            reader.readAsDataURL(archivo);
            reader.onload=function(){
                var aux=[];
                var base64 = reader.result;
                //imagen = base64;
                console.log("a base 64");
                console.log(base64);
                aux = base64.split(',');
                enBase64 = aux[1];
                console.log(enBase64);
                var aux2, aux3 = [];
                aux2 =aux[0].split('/');
                aux3 = aux2[1].split(';');
                ext = aux3[0]
                extension();
                console.log('la extension es: ' + ext);
                console.log("archivo " + archivoNombre.name);
            }
        })
    }

    const extension = ()=>{
        if(ext === "vnd.openxmlformats-officedocument.wordprocessingml.document"){
            ext = "docx"
        }else if(ext === "vnd.openxmlformats-officedocument.presentationml.presentation"){
            ext = "pptx"
        }else if(ext == "vnd.openxmlformats-officedocument.spreadsheetml.sheet"){
            ext = "xlsx"
        }else if(ext == "msword"){
            ext = "doc"
        }
    }

    const crearArchivo = async()=>{
        console.log("vamos a crear archivo")
        if(enBase64 != '' && ext != '' && archivoNombre.name != ''){
            axios.post(url+"s3/crearnuevoarchivosinrepetir",{name: archivoNombre.name, ext: ext, name2: cookies.get("name")+"/"+name + '/', base64: enBase64})
            .then(response=>{
                var resp = { "RESPUESTA": "hola"};
                resp = response.data
                console.log(resp.RESPUESTA)
                if(resp.RESPUESTA == "SI SE CREO/"){
                    console.log("archivo creado")
                    obtenerURL();
                    setTimeout(`location.href='./${name}'`, 1000);
                }else{
                    Swal.fire({
                        icon: 'error',
                        title: 'Error',
                        showConfirmButton: false,
                        timer: 2000
                    })
                    console.log("archivo no creado")
                }
            })
            .catch(error=>{
                console.log(error);
            })
        }
    }

    let URL = ''
    const obtenerURL = async()=>{
        axios.post(url+"s3/obtenerURL",{name: archivoNombre.name + '.' + ext, dire: cookies.get("name")+"/"+name+ '/'})
        .then(response=>{
            var resp = { "Direccion": "hola"};
            resp = response.data
            console.log(resp.Direccion)
            URL = resp.Direccion
            saveFile();
        })
        .catch(error=>{
            console.log(error);
        })
    }

    const saveFile = async()=>{
        var f = new Date();
        var fechaactual = f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear();
        axios.post(url+"ddb/archivo/save",{nombre: archivoNombre.name, extension: ext, propietario: cookies.get("name"), carpeta: name, fecha: fechaactual, link: URL})
        .then(response=>{
            var resp = { "RESPUESTA": "hola"};
            resp = response.data
            console.log(resp.RESPUESTA)
            if(resp.RESPUESTA == "EXITOSO/"){
                console.log("archivo creado en bd")
            }else{
                console.log("archivo no creado en bd")
            }
        })
        .catch(error=>{
            console.log(error);
        })
    }

    return(
        <>
            <NavBar2/>
            <div className={classes.root}>
                <Paper elevation={10} className={classes.paperStyle}>
                    <Grid aling="left">
                        <Tooltip title="New File">
                            <Button onClick={handleOpen3}>
                                <NoteAddIcon style={{ color: '#6497b1', fontSize: 40 }} />
                            </Button>
                        </Tooltip>
                        <Modal
                        open={open3}
                            onClose={handleClose3}
                        >
                            <div style={modalStyle} className={classes.paper}>
                                <Typography variant="h5" color="initial" align="center">New File</Typography>
                                <Divider className={classes.divider2}/>
                                <TextField onChange={handleInputChangeArchivo} name="name" placeholder="Please enter name." label="Name" fullWidth required></TextField>
                                <input className={classes.custom} data-input="false" type="file" id="Photo" accept="image/*, application/pdf, .xlsx,.xls, .doc, .docx, .ppt, .pptx,.txt, .mp4, text/plain" multiple onChange={(e)=>convertirBase64(e.target.files)}></input> 
                                <Divider className={classes.divider2}/>
                                <Button onClick={crearArchivo} className={classes.btn} type="submit"  variant="contained" fullWidth>Create</Button>
                        
                            </div>
                        </Modal>


                        <Tooltip title="Delete File">
                            <Button onClick={abrirEliminarArchivo}>
                                <DeleteIcon style={{ color: '#6497b1', fontSize: 40 }} />
                            </Button>
                        </Tooltip>
                        <Modal
                        open={openEliminarArchivo}
                        onClose={cerrarEliminarArchivo}
                        >
                            <div style={modalStyle} className={classes.paper}>
                                <Typography variant="h5" color="initial" align="center">Delete File</Typography>
                                <Divider className={classes.divider2}/>
                                <FormControl className={classes.formControl} variant="filled">
                                    <InputLabel >File</InputLabel>
                                    <Select
                                    className={classes.selectEmpty}
                                    labelId="demo-simple-select-filled-label"
                                    id="demo-simple-select-filled"
                                    value={file}
                                    onChange={seleccionarArchivo}
                                    fullWidth
                                    >
                                    <MenuItem value="">
                                        <em>None</em>
                                    </MenuItem>
                                    {datos2.map((a,i)=>{
                                        return(
                                            <MenuItem value={a.codigo}>{a.nombre}</MenuItem>
                                        )
                                    })}
                                    </Select>
                                </FormControl>
                                <Divider className={classes.divider}/>
                                <Button onClick={eliminarArchivo} className={classes.btn} type="submit"  variant="contained" >Delete</Button>
                        
                            </div>
                        </Modal>

                            <Tooltip title="Update File">
                                <Button onClick={handleOpenActualizar}>
                                    <UpdateIcon style={{ color: '#6497b1', fontSize: 40 }} />
                                </Button>
                            </Tooltip>
                            <Modal
                            open={open4}
                                onClose={handleCloseActualizar}
                            >
                                <div style={modalStyle} className={classes.paper}>
                                    <Typography variant="h5" color="initial" align="center">Update File</Typography>
                                    <Divider className={classes.divider2}/>
                                    <TextField onChange={handleInputChangeNuevoNombre} name="name" placeholder="Please enter  a new name." label="name" fullWidth required></TextField>
                                    
                                    <FormControl className={classes.formControl} variant="filled">
                                        <InputLabel >Select File</InputLabel>
                                        <Select
                                        className={classes.selectEmpty}
                                        labelId="demo-simple-select-filled-label"
                                        id="demo-simple-select-filled"
                                        value={archivoUpdate}
                                        onChange={seleccionarArchivoUpdate}
                                        fullWidth
                                        >
                                        <MenuItem value="">
                                            <em>None</em>
                                        </MenuItem>
                                        {datos2.map((a,i)=>{
                                            return(
                                                <MenuItem value={a.codigo}>{a.nombre}</MenuItem>
                                            )
                                        })}
                                        </Select>
                                    </FormControl>

                                    <Divider className={classes.divider2}/>
                                    <Button onClick={updateArchivo} className={classes.btn} type="submit"  variant="contained" fullWidth>Update</Button>
                                </div>
                            </Modal>



                            <Tooltip title="Move File">
                            <Button onClick={abrirMoverArchivo} >
                                <SwapHorizontalCircleIcon style={{ color: '#6497b1', fontSize: 40 }} />
                            </Button>
                            </Tooltip>
                            <Modal
                            open={openMoverArchivo}
                            onClose={cerrarMoverArchivo}
                            >
                                <div style={modalStyle} className={classes.paper}>
                                    <Typography variant="h5" color="initial" align="center">Move File</Typography>
                                    <Divider className={classes.divider2}/>
                                    <FormControl className={classes.formControl} variant="filled">
                                        <Select
                                        className={classes.selectEmpty}
                                        //labelId="demo-simple-select-filled-label"
                                        id="demo-simple-select-filled"
                                        value={ArchivoMover}
                                        onChange={seleccionarArchivoMover}
                                        fullWidth
                                        >
                                        <MenuItem value="">
                                            <em>None</em>
                                        </MenuItem>
                                        {datos2.map((a,i)=>{
                                            return(
                                                <MenuItem value={a.codigo}>{a.nombre}</MenuItem>
                                            )
                                        })}
                                        </Select>
                                        <FormHelperText>Select File</FormHelperText>
                                        
                                        <Select
                                        className={classes.selectEmpty}
                                        //labelId="demo-simple-select-filled-label"
                                        id="demo-simple-select-filled"
                                        value={Carpeta2}
                                        onChange={seleccionarCarpeta2}
                                        fullWidth
                                        >
                                        <MenuItem value="">
                                            <em>None</em>
                                        </MenuItem>
                                        {datos.map((a,i)=>{
                                            return(
                                                <MenuItem value={a.nombre}>{a.nombre}</MenuItem>
                                            )
                                        })}
                                        </Select>
                                        <FormHelperText>Select Folder</FormHelperText>
                                    </FormControl>
                                    <Divider className={classes.divider}/>
                                    <Button onClick={moverArchivo} className={classes.btn} type="submit"  variant="contained" >Move File</Button>
                                </div>
                            </Modal>

                    </Grid>
                </Paper>


                <Container maxWidth="lg" className={classes.container}>
                    <Typography variant="h5" color="initial" align="center">{name} Files (Click to download)</Typography>
                    <Divider  flexItem />
                    {datos2.map((a,i)=>{
                        return(
                            <div key={i+100} className={classes.rootAcordeon}>
                                <Accordion expanded={expanded === i+100} onChange={handleChange(i+100)}>
                                    <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel1bh-content"
                                    id="panel1bh-header"
                                    >
                                    <Grid item xs={11}>
                                        <a className={classes.link} href={a.link}>
                                            <Grid container>
                                                <Grid item xs={1}>
                                                    <InsertDriveFileIcon style={{color: '#6497b1', fontSize: 40}}/>
                                                </Grid>
                                                <Grid item xs={11}>
                                                    <div className={classes.letritas}>{a.nombre}.{a.extension}</div>
                                                </Grid>
                                            </Grid>
                                        </a>
                                    </Grid>
                                    </AccordionSummary>
                                    <AccordionDetails>
                                    <Grid aling="center">
                                    
                                                <p>----------------- Properties -----------------</p>
                                                <Divider/>
                                                <p>Name: {datos2[i].nombre}</p>
                                                <p>Extension: {datos2[i].extension}</p>
                                                <p>Date: {datos2[i].fecha}</p>
                                                <p>Code: {datos2[i].codigo}</p>
                                                <p>Owner: {datos2[i].propietario}</p>
                                                <p>Link: {datos2[i].link}</p>
                                                <p>Folder: {datos2[i].carpeta}</p>
                                                <Divider/>
                                    </Grid>
                                    </AccordionDetails>
                                </Accordion>   
                            </div>
                        )
                    })}
                </Container>
            </div>
        </>
    )
}
