import React, {useState, useEffect} from 'react'
import {Grid, Button, FormGroup, FormControlLabel, Paper, TextField} from '@material-ui/core'
import Checkbox from '@material-ui/core/Checkbox';
import {makeStyles}  from '@material-ui/core/styles';
import axios from 'axios';
import Swal from 'sweetalert2';
import Cookies from 'universal-cookie';
import { useStopwatch } from 'react-timer-hook';
 
const cookies = new Cookies();
const random = require('string-random')

const url = "http://34.71.225.235:3000/";

var md5 = require('md5');

const useStyles = makeStyles((theme) => ({
    root: {
        position: 'relative',
        backgroundColor: "#b3cde0",
        paddingTop: '20px',
        paddingLeft: '100px',
        paddingRight: '100px',
        paddingBottom: '50px'
    },
    imagen: {
        width: '475px'
    },
    divider: {
        margin: theme.spacing(1, 0),
    },
    paperStyle: {
        padding: 20,
        height: "75vh",
        width: 290,
        margin: "20px auto"
    },
    btn: {
        margin: '15px 0',
        backgroundColor: '#005b96',
        color: 'white'
    },
    avatar:{
        backgroundColor: '#6497b1',
        width: 60,
        height: 60
    }
  }));

const Auth = () =>{
    useEffect(() => {
        if(!cookies.get('name')){
            window.location.href='./';
        }
    },[]);

    //creamos token random el cual no debe de cambiar
    const [tokenR,setTokenR] = useState(random(5));

    const {
        seconds,
        minutes,
        start,
      } = useStopwatch({ autoStart: false });
    
    const classes = useStyles();

    const [datos, setDatos] = useState({
        token: ''
    })

    const handleInputChange = (event) =>{
        
        setDatos({
            ...datos,
            [event.target.name]: event.target.value
        })
    }

    //state para caja desactivdada de token
    const [ Caja, setCaja] = useState(true);

    const enviarDatos = (event) =>{
        console.log("Check correo:" + checked );
        console.log("Check numero:" + checkedN );
        if(checked === false && checkedN === false){
            Swal.fire({
                icon: 'error',
                title: 'Choose one or both options',
                showConfirmButton: false,
                timer: 2000
            });
        }else{
            //desabilitamos el boton
            setBotonSend(true);
            entrar();
            
            //iniciamos contador, mostramos mensaje y contamos 1 minuto antes de reiniciar la pagina.
            start();
            setCaja(false);

            setTimeout(notificar, 60000);
        }
    }

    //UseState para check de correo electronico
    const [checked, setCheckedE] = useState(false);

    const handleChangeE = (event) => {
      setCheckedE(event.target.checked);
    };

    //UseState para check de numero de telefono
    const [checkedN, setCheckedN] = useState(false);

    const handleChangeN = (event) => {
      setCheckedN(event.target.checked);
    };

    const [ BotonSend, setBotonSend] = useState(false);

    //{enviar token de autenticacion
    const entrar = async()=>{
        //correo
        var correo = "";
        if(checked == true){
            correo = "si";
        }else{
            correo = "no";
        }
        //celular
        var celular = "";
        if(checkedN == true){
            celular = "si";
        }else{
            celular = "no";
        }

        var usuario = cookies.get('name');
        axios.post(url+"ddb/usuario/Autenticacion", {"id": usuario, "token": tokenR, "cel": celular, "correo": correo })
        .then(response=>{
            console.log(usuario +" "+ tokenR +" "+ celular + correo);
            var resp = { "RESPUESTA": "hola"};
            console.log(resp.RESPUESTA)
            resp = response.data
            console.log(resp.RESPUESTA)
            if(resp.RESPUESTA == "EXITOSO/"){
                Swal.fire({
                    icon: 'info',
                    title: 'You have 2 minutes to enter the token.',
                    showConfirmButton: false,
                    timer: 3000
                })
                                
            }else{
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    showConfirmButton: false,
                    timer: 2000
                })
            }
        })
        .catch(error=>{
            console.log(error);
        })
    }

    const avisar = async()=>{
        if(datos.token == tokenR){
            Swal.fire({
                icon: 'success',
                title: 'Welcome',
                showConfirmButton: false,
                timer: 2000
                
            });
            setTimeout("location.href='./profile'", 500);
        }else{
            Swal.fire({
                icon: 'error',
                title: 'Try Again',
                showConfirmButton: false,
                timer: 2000
            });
            setTimeout("location.href='./login_token'", 500);
        }
    }

    const notificar = (event)=>{
        Swal.fire({
            icon: 'error',
            title: 'Try Again',
            showConfirmButton: false,
            timer: 2000
        });
        setTimeout(irToken, 2800);
    }

    const irToken=()=>{
        window.location.href='/login_token';
    }

//1.6.0    
    const wi = "#FFFFF";
    var perfil = cookies.get('name') + tokenR;
    return(
        <>
            <div className={classes.root}>
                <Grid>
                    <Paper elevation={10} className={classes.paperStyle}>
                        <Grid align="center">
                            <h2>Authentication method</h2>
                        </Grid>
                        <h4>Select verification method:</h4>
                        <FormGroup>
                            <FormControlLabel checked={checked} onChange={handleChangeE} control={<Checkbox />} label="E-mail" />
                            <FormControlLabel checked={checkedN} onChange={handleChangeN} control={<Checkbox />} label="Phone Number" />
                        </FormGroup>
                        <Button onClick={enviarDatos} disabled={BotonSend} className={classes.btn} type="submit"  variant="contained" fullWidth>Send Token</Button>   
                        <h4>Current Time:</h4>
                        <Grid align="center">
                        <h2  color={wi} >{minutes}:{seconds}</h2>
                        </Grid>
                        <TextField onChange={handleInputChange} disabled={Caja} name="token" placeholder="Received Token." label="Token" fullWidth required></TextField>                  
                        <Button onClick={avisar}  disabled={Caja} className={classes.btn} type="submit"  variant="contained" fullWidth>Verify Token</Button>   
                        
                    </Paper>
                </Grid>
            </div>
        </>
    )
}

export default Auth